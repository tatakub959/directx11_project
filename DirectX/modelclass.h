#pragma once
#include "d3d11class.h"
class ModelClass {
public:

	//Cube
	XMMATRIX cube1World;
	XMMATRIX cube2World;
	XMMATRIX cube3World;

	XMMATRIX RotationX;
	XMMATRIX RotationY;
	XMMATRIX RotationZ;
	XMMATRIX Scale;
	XMMATRIX Translation;
	float rot = 0.01f;

	//Get from Input
	float rotx;
	float rotz;
	float scaleX;
	float scaleY;

	XMMATRIX Rotationx;
	XMMATRIX Rotationz;

	// leaf data (leaves are drawn as quads)
	ID3D11Buffer *quadVertBuffer;
	ID3D11Buffer *quadIndexBuffer;

	struct Vertex
	{
		Vertex() {}
		Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, float tx, float ty, float tz) :
			pos(x, y, z), texCoord(u, v), normal(nx, ny, nz), tangent(tx, ty, tz) {}

		XMFLOAT3 pos;
		XMFLOAT2 texCoord;
		XMFLOAT3 normal;

		XMFLOAT3 tangent;
		XMFLOAT3 biTangent;
	};


public:

	void initModel(ID3D11Device* d3d11Device, ID3D11DeviceContext* d3d11DevCon, ID3D10Blob* VS_Buffer);
	void UpdateModel(XMVECTOR camPosition);
	void CleanUp();

};