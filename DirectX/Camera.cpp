#include "cameraclass.h"

CameraClass::CameraClass(ID3D11Device* d3d11DeviceMain, ID3D11DeviceContext* d3d11DevConMain) {
	d3d11Device = d3d11DeviceMain;
	d3d11DevCon = d3d11DevConMain;

}

void CameraClass:: InitCamera(int Width, int Height /*, std::vector<XMFLOAT3> collidableGeometryPositionsM, std::vector<DWORD> collidableGeometryIndicesM*/ ) {

	//collidableGeometryPositions = collidableGeometryPositionsM;
	//collidableGeometryIndices = collidableGeometryIndicesM;

	//Create the Viewport
	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = Width;
	viewport.Height = Height;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;

	//Set the Viewport
	d3d11DevCon->RSSetViewports(1, &viewport);

	//Create the buffer to send to the cbuffer in effect file
	D3D11_BUFFER_DESC cbbd;
	ZeroMemory(&cbbd, sizeof(D3D11_BUFFER_DESC));

	cbbd.Usage = D3D11_USAGE_DEFAULT;
	cbbd.ByteWidth = sizeof(cbPerObject);
	cbbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbbd.CPUAccessFlags = 0;
	cbbd.MiscFlags = 0;

	d3d11Device->CreateBuffer(&cbbd, NULL, &cbPerObjectBuffer);

	/************************************New Stuff****************************************************/
	//Create the buffer to send to the cbuffer per instance in effect file
	ZeroMemory(&cbbd, sizeof(D3D11_BUFFER_DESC));

	cbbd.Usage = D3D11_USAGE_DEFAULT;
	cbbd.ByteWidth = sizeof(cbPerScene);
	cbbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbbd.CPUAccessFlags = 0;
	cbbd.MiscFlags = 0;

	d3d11Device->CreateBuffer(&cbbd, NULL, &cbPerInstanceBuffer);

	d3d11DevCon->UpdateSubresource(cbPerInstanceBuffer, 0, NULL, &cbPerInst, 0, 0);
	/*************************************************************************************************/

	//Camera information
	camPosition = XMVectorSet(0.0f, 10.0f, -50.0f, 0.0f);
	camTarget = XMVectorSet(0.0f, 3.0f, 0.0f, 0.0f);
	camUp = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	//Set the View matrix
	camView = XMMatrixLookAtLH(camPosition, camTarget, camUp);

	//Set the Projection matrix
	camProjection = XMMatrixPerspectiveFovLH(0.4f*3.14f, (float)Width / Height, 1.0f, 1000.0f);
	//camProjection = XMMatrixPerspectiveFovLH(3.14f / 4.0f, (float)Width / Height, 1.0f, 200.0f);

	//m_GroundCollision = new GroundCollisionClass;

}

void CameraClass::DrawCamera(XMMATRIX cubeWorld , ID3D11ShaderResourceView* CubesTexture, ID3D11SamplerState* CubesTexSamplerState , ID3D11RasterizerState* CWcullMode , UINT index, BOOL hasTexture, BOOL hasNormMap) {

	WVP = cubeWorld * camView * camProjection;
	cbPerObj.World = XMMatrixTranspose(cubeWorld);
	cbPerObj.WVP = XMMatrixTranspose(WVP);
	cbPerObj.hasTexture = hasTexture;
	cbPerObj.hasNormMap = hasNormMap;
	cbPerObj.isInstance = false;
	cbPerObj.isGrass = false;
	d3d11DevCon->UpdateSubresource(cbPerObjectBuffer, 0, NULL, &cbPerObj, 0, 0);
	d3d11DevCon->VSSetConstantBuffers(0, 1, &cbPerObjectBuffer);
	d3d11DevCon->PSSetShaderResources(0, 1, &CubesTexture);
	d3d11DevCon->PSSetSamplers(0, 1, &CubesTexSamplerState);
	d3d11DevCon->RSSetState(CWcullMode);
	//Draw a model
	d3d11DevCon->DrawIndexed(index, 0, 0);

}

void CameraClass::MovingCamera(float moveLeftRight, float moveBackForward, float camYaw, float camPitch, bool isJump) {

	camRotationMatrix = XMMatrixRotationRollPitchYaw(camPitch, camYaw, 0);
	camTarget = XMVector3TransformCoord(DefaultForward, camRotationMatrix);
	camTarget = XMVector3Normalize(camTarget);
	
	
	RotateYTempMatrix = XMMatrixRotationY(camYaw);
	
	//FPS-Camera
	camRight = XMVector3TransformCoord(DefaultRight, RotateYTempMatrix);
	camUp = XMVector3TransformCoord(camUp, RotateYTempMatrix);
	camForward = XMVector3TransformCoord(DefaultForward, RotateYTempMatrix);


	//Free-Look Camera
	//camRight = XMVector3TransformCoord(DefaultRight, camRotationMatrix);
	//camUp = XMVector3Cross(camForward, camRight);
	//camForward = XMVector3TransformCoord(DefaultForward, camRotationMatrix);
	
	//Add ground collision----------------------------

	//GroundCollisionClass::CollisionPacket cameraCP;
	//cameraCP.ellipsoidSpace = XMVectorSet(1.0f, 10.0f, 1.0f, 0.0f);
	//cameraCP.w_Position = camPosition;
	//cameraCP.w_Velocity = (moveLeftRight*camRight) + (moveBackForward*camForward);

	////GroundCollisionClass::CollisionPacket cameraCP_ground;
	////cameraCP_ground.ellipsoidSpace = XMVectorSet(1.0f, 10.0f, 1.0f, 0.0f);
	////cameraCP_ground.w_Position = camPosition;
	////cameraCP_ground.w_Velocity = (moveLeftRight*camRight) + (moveBackForward*camForward);

	//if (isJump) {

	//	m_GroundCollision->gravity = XMVectorSet(0.0f, 0.6f, 0.0f, 0.0f);
	//}

	//else
	//{
	//	m_GroundCollision->gravity = XMVectorSet(0.0f, -0.5f, 0.0f, 0.0f);
	//}

	//camPosition = m_GroundCollision->CollisionSlide(cameraCP,collidableGeometryPositions,collidableGeometryIndices);

	//--------------------------------------------------------------

	camPosition += moveLeftRight*camRight;
	camPosition += moveBackForward*camForward;
	
	camTarget = camPosition + camTarget;
	
	camView = XMMatrixLookAtLH(camPosition, camTarget, camUp);

}


void CameraClass::CleanUp() {

	d3d11Device->Release();
	d3d11DevCon->Release();
	cbPerObjectBuffer->Release();
	cbPerInstanceBuffer->Release();

}