#pragma once
#include "d3d11class.h"

class RenderStateClass{

public:
	RenderStateClass(ID3D11Device* d3d11Device, ID3D11DeviceContext* d3d11DevCon);
	void SetRenderState();
	void CleanUp();

private:
	ID3D11Device* d3d11Device;
	ID3D11DeviceContext* d3d11DevCon;

	ID3D11RasterizerState* WireFrame;
};