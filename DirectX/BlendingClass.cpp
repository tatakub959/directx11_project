#include "blendingclass.h"

BlendingClass::BlendingClass(ID3D11Device* d3d11DeviceMain) {
	d3d11Device = d3d11DeviceMain;
}

void BlendingClass::initBlending() {

	//Define the Blending Equation
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));

	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
	ZeroMemory(&rtbd, sizeof(rtbd));

	rtbd.BlendEnable = true;
	rtbd.SrcBlend = D3D11_BLEND_SRC_COLOR;
	rtbd.DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	rtbd.BlendOp = D3D11_BLEND_OP_ADD;
	rtbd.SrcBlendAlpha = D3D11_BLEND_ONE;
	rtbd.DestBlendAlpha = D3D11_BLEND_ZERO;
	rtbd.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	rtbd.RenderTargetWriteMask = D3D10_COLOR_WRITE_ENABLE_ALL;

	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.RenderTarget[0] = rtbd;

	d3d11Device->CreateBlendState(&blendDesc, &Transparency);

	ZeroMemory(&cmdesc, sizeof(D3D11_RASTERIZER_DESC));

	//cmdesc.FillMode = D3D11_FILL_WIREFRAME;
	cmdesc.FillMode = D3D11_FILL_SOLID;
	//cmdesc.CullMode = D3D11_CULL_NONE;
	//cmdesc.CullMode = D3D11_CULL_FRONT;
	cmdesc.CullMode = D3D11_CULL_BACK;

	cmdesc.FrontCounterClockwise = true;
	d3d11Device->CreateRasterizerState(&cmdesc, &CCWcullMode);

	cmdesc.FrontCounterClockwise = false;
	d3d11Device->CreateRasterizerState(&cmdesc, &CWcullMode);

}

void BlendingClass::CleanUp() {

	Transparency->Release();
	CCWcullMode->Release();
	CWcullMode->Release();
}