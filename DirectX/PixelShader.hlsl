struct PS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float4 Color : COLOR;
};

float4 PS(PS_OUTPUT input) : SV_TARGET
{
	return input.Color;
}