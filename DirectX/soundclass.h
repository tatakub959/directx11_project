#pragma once
#include "d3d11class.h"

class SoundClass {
public:
	struct WaveHeaderType
	{
		char chunkId[4];
		unsigned long chunkSize;
		char format[4];
		char subChunkId[4];
		unsigned long subChunkSize;
		unsigned short audioFormat;
		unsigned short numChannels;
		unsigned long sampleRate;
		unsigned long bytesPerSecond;
		unsigned short blockAlign;
		unsigned short bitsPerSample;
		char dataChunkId[4];
		unsigned long dataSize;
	};

public:
	bool Initialize(HWND);
	bool InitializeDirectSound(HWND);
	void ShutdownDirectSound();

	bool LoadWaveFile(char*, IDirectSoundBuffer8**);
	void ShutdownWaveFile(IDirectSoundBuffer8**);

	bool PlayWaveFile();
	void SoundPlay(bool isWaling , bool isRunning , bool isPick , bool isSpawn, bool isJumpScare , bool HeartIsBeating);
	void StopSound();

	void CleanUp();

public:
	IDirectSound8* m_DirectSound;
	IDirectSoundBuffer* m_primaryBuffer;
	IDirectSoundBuffer8* m_secondaryBuffer1;
	IDirectSoundBuffer8* m_secondaryBuffer2_Walk;
	IDirectSoundBuffer8* m_secondaryBuffer3_Run;
	IDirectSoundBuffer8* m_secondaryBuffer4_Pick;
	IDirectSoundBuffer8* m_secondaryBuffer5_GhostSpawn;
	IDirectSoundBuffer8* m_secondaryBuffer6_JumpScare;
	IDirectSoundBuffer8* m_secondaryBuffer7_HeartBeat;
	IDirectSoundBuffer8* m_secondaryBuffer8_Breathing;

};