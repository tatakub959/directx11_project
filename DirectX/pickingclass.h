#pragma once
#include "d3d11class.h"


class PickingClass {
public:

	HWND hwnd;

	int ClientWidth = 0;
	int ClientHeight = 0;

	int score = 0;
	float pickedDist = 0.0f;
	bool isPick = false;

	bool isInUIMode = false;


public:

	PickingClass(HWND hwnd, int ClientWidth, int ClientHeight);

	void pickRayVector(float mouseX, float mouseY, XMVECTOR& pickRayInWorldSpacePos, XMVECTOR& pickRayInWorldSpaceDir, XMMATRIX camProjection, XMMATRIX camView);
	float pick(XMVECTOR pickRayInWorldSpacePos, XMVECTOR pickRayInWorldSpaceDir, std::vector<XMFLOAT3>& vertPosArray, std::vector<DWORD>& indexPosArray, XMMATRIX& worldSpace);
	bool PointInTriangle(XMVECTOR& triV1, XMVECTOR& triV2, XMVECTOR& triV3, XMVECTOR& point);

	void MouseClickDetectForPicking(bool LeftClick, bool ResetPicking, int numBottles, int* bottleHit, std::vector<XMFLOAT3> bottleVertPosArray,std::vector<DWORD> bottleVertIndexArray,
							XMMATRIX bottleWorld[], XMMATRIX camProjection, XMMATRIX camView, XMVECTOR bottleCenterOffest, float bottleBoundingSphere, int pickWhat,
							std::vector<XMFLOAT3> bottleBoundingBoxVertPosArray, std::vector<DWORD> bottleBoundingBoxVertIndexArray);

	void MouseClickUI(bool isInUIMode, bool LeftClick ,int numBottles, int* bottleHit,XMMATRIX bottleWorld[], XMMATRIX camProjection, XMMATRIX camView,
						std::vector<XMFLOAT3> bottleBoundingBoxVertPosArray, std::vector<DWORD> bottleBoundingBoxVertIndexArray);
};