#include "pickingclass.h"
PickingClass::PickingClass(HWND hwndM, int ClientWidthM, int ClientHeightM) {
	hwnd = hwndM;
	ClientWidth = ClientWidthM;
	ClientHeight = ClientHeightM;
}

void PickingClass::MouseClickDetectForPicking(bool LeftClick, bool ResetPicking, int numBottles, int* bottleHit, std::vector<XMFLOAT3> bottleVertPosArray, std::vector<DWORD> bottleVertIndexArray, XMMATRIX bottleWorld[]
	,XMMATRIX camProjection, XMMATRIX camView, XMVECTOR bottleCenterOffset , float bottleBoundingSphere, int pickWhat, std::vector<XMFLOAT3> bottleBoundingBoxVertPosArray, std::vector<DWORD> bottleBoundingBoxVertIndexArray) {

	if (LeftClick) {
		POINT mousePos;

		GetCursorPos(&mousePos);
		ScreenToClient(hwnd, &mousePos);

		int mousex = mousePos.x;
		int mousey = mousePos.y;

		float tempDist;
		float closestDist = FLT_MAX;
		int hitIndex;

		XMVECTOR prwsPos, prwsDir;
		pickRayVector(mousex, mousey, prwsPos, prwsDir, camProjection, camView);

		for (int i = 0; i < numBottles; i++)
		{

			if (bottleHit[i] == 0)
			{
				tempDist = FLT_MAX;

				//Bounding Sphere
				if (pickWhat == 1) {
					float pRToPointDist = 0.0f;

					XMVECTOR bottlePos = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
					XMVECTOR pOnLineNearBottle = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);

					bottlePos = XMVector3TransformCoord(bottlePos, bottleWorld[i]) + bottleCenterOffset;

					pOnLineNearBottle = prwsPos + XMVector3Dot((bottlePos - prwsPos), prwsDir) / XMVector3Dot(prwsDir, prwsDir) * prwsDir;

					pRToPointDist = XMVectorGetX(XMVector3Length(pOnLineNearBottle - bottlePos));

					if (pRToPointDist < bottleBoundingSphere) {
							
						tempDist = pick(prwsPos, prwsDir, bottleVertPosArray, bottleVertIndexArray, bottleWorld[i]);
					}
				}

				//Bounding box
				if (pickWhat == 0) {
					tempDist = pick(prwsPos, prwsDir, bottleBoundingBoxVertPosArray, bottleBoundingBoxVertIndexArray, bottleWorld[i]);
				}

				//No Bounding
				if (pickWhat == 2) {
					tempDist = pick(prwsPos, prwsDir, bottleVertPosArray, bottleVertIndexArray, bottleWorld[i]);
				}

				if (tempDist < closestDist) {
					closestDist = tempDist;
					hitIndex = i;
				}
			}
		}

		//if (closestDist < FLT_MAX)
		if (closestDist <= 10.0f)
		{	
				bottleHit[hitIndex] = 1;
				pickedDist = closestDist;
				score++;
				isPick = true;

		}

	}

	if (!LeftClick)
		isPick = false;

}

void PickingClass::MouseClickUI(bool isInUIModeM, bool LeftClick, int numBottles, int* bottleHit, XMMATRIX bottleWorld[], XMMATRIX camProjection, XMMATRIX camView,
	std::vector<XMFLOAT3> bottleBoundingBoxVertPosArray, std::vector<DWORD> bottleBoundingBoxVertIndexArray)
{
	isInUIMode = isInUIModeM;

	if (LeftClick) {
		POINT mousePos;

		GetCursorPos(&mousePos);
		ScreenToClient(hwnd, &mousePos);

		int mousex = mousePos.x;
		int mousey = mousePos.y;

		float tempDist;
		float closestDist = FLT_MAX;
		int hitIndex;

		XMVECTOR prwsPos, prwsDir;
		pickRayVector(mousex, mousey, prwsPos, prwsDir, camProjection, camView);

		for (int i = 1; i < numBottles; i++)
		{

			if (bottleHit[i] == 0)
			{
				tempDist = FLT_MAX;

				//Bounding box
				tempDist = pick(prwsPos, prwsDir, bottleBoundingBoxVertPosArray, bottleBoundingBoxVertIndexArray, bottleWorld[i]);
			
				if (tempDist < closestDist) {
					closestDist = tempDist;
					hitIndex = i;
				}
			}
		}

		if (closestDist < FLT_MAX)
		//if (closestDist <= 10.0f)
		{
			bottleHit[hitIndex] = 1;
			pickedDist = closestDist;
			isPick = true;

		}

	}

	if (!LeftClick)
		isPick = false;
	
}

void PickingClass::pickRayVector(float mouseX, float mouseY, XMVECTOR& pickRayInWorldSpacePos, XMVECTOR& pickRayInWorldSpaceDir, XMMATRIX camProjection, XMMATRIX camView) {
	XMVECTOR pickRayInViewSpaceDir = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	XMVECTOR pickRayInViewSpacePos = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);

	float PRVecX, PRVecY, PRVecZ;

	//Transform 2D pick position on screen space to 3D ray in View space
	PRVecX = (((2.0f * mouseX) / ClientWidth) - 1) / camProjection(0, 0);
	PRVecY = -(((2.0f * mouseY) / ClientHeight) - 1) / camProjection(1, 1);
	PRVecZ = 1.0f;    //View space's Z direction ranges from 0 to 1, so we set 1 since the ray goes "into" the screen
	
	if(isInUIMode)
		pickRayInViewSpaceDir = XMVectorSet(PRVecX, PRVecY, PRVecZ, 0.0f);

	//Uncomment this line if you want to use the center of the screen (client area)
	//to be the point that creates the picking ray (eg. first person shooter)
	
	if (!isInUIMode)
		pickRayInViewSpaceDir = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);

	// Transform 3D Ray from View space to 3D ray in World space
	XMMATRIX pickRayToWorldSpaceMatrix;
	XMVECTOR matInvDeter;    //We don't use this, but the xna matrix inverse function requires the first parameter to not be null

	pickRayToWorldSpaceMatrix = XMMatrixInverse(&matInvDeter, camView);    //Inverse of View Space matrix is World space matrix

	pickRayInWorldSpacePos = XMVector3TransformCoord(pickRayInViewSpacePos, pickRayToWorldSpaceMatrix);
	pickRayInWorldSpaceDir = XMVector3TransformNormal(pickRayInViewSpaceDir, pickRayToWorldSpaceMatrix);

}

float PickingClass::pick(XMVECTOR pickRayInWorldSpacePos, XMVECTOR pickRayInWorldSpaceDir, std::vector<XMFLOAT3>& vertPosArray, std::vector<DWORD>& indexPosArray, XMMATRIX& worldSpace) {

	//Loop through each triangle in the object
	for (int i = 0; i < indexPosArray.size() / 3; i++)
	{
		//Triangle's vertices V1, V2, V3
		XMVECTOR tri1V1 = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
		XMVECTOR tri1V2 = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
		XMVECTOR tri1V3 = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);

		//Temporary 3d floats for each vertex
		XMFLOAT3 tV1, tV2, tV3;

		//Get triangle 
		tV1 = vertPosArray[indexPosArray[(i * 3) + 0]];
		tV2 = vertPosArray[indexPosArray[(i * 3) + 1]];
		tV3 = vertPosArray[indexPosArray[(i * 3) + 2]];

		tri1V1 = XMVectorSet(tV1.x, tV1.y, tV1.z, 0.0f);
		tri1V2 = XMVectorSet(tV2.x, tV2.y, tV2.z, 0.0f);
		tri1V3 = XMVectorSet(tV3.x, tV3.y, tV3.z, 0.0f);

		//Transform the vertices to world space
		tri1V1 = XMVector3TransformCoord(tri1V1, worldSpace);
		tri1V2 = XMVector3TransformCoord(tri1V2, worldSpace);
		tri1V3 = XMVector3TransformCoord(tri1V3, worldSpace);

		//Find the normal using U, V coordinates (two edges)
		XMVECTOR U = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
		XMVECTOR V = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
		XMVECTOR faceNormal = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);

		U = tri1V2 - tri1V1;
		V = tri1V3 - tri1V1;

		//Compute face normal by crossing U, V
		faceNormal = XMVector3Cross(U, V);

		faceNormal = XMVector3Normalize(faceNormal);

		//Calculate a point on the triangle for the plane equation
		XMVECTOR triPoint = tri1V1;

		//Get plane equation ("Ax + By + Cz + D = 0") Variables
		float tri1A = XMVectorGetX(faceNormal);
		float tri1B = XMVectorGetY(faceNormal);
		float tri1C = XMVectorGetZ(faceNormal);
		float tri1D = (-tri1A*XMVectorGetX(triPoint) - tri1B*XMVectorGetY(triPoint) - tri1C*XMVectorGetZ(triPoint));

		//Now we find where (on the ray) the ray intersects with the triangles plane
		float ep1, ep2, t = 0.0f;
		float planeIntersectX, planeIntersectY, planeIntersectZ = 0.0f;
		XMVECTOR pointInPlane = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);

		ep1 = (XMVectorGetX(pickRayInWorldSpacePos) * tri1A) + (XMVectorGetY(pickRayInWorldSpacePos) * tri1B) + (XMVectorGetZ(pickRayInWorldSpacePos) * tri1C);
		ep2 = (XMVectorGetX(pickRayInWorldSpaceDir) * tri1A) + (XMVectorGetY(pickRayInWorldSpaceDir) * tri1B) + (XMVectorGetZ(pickRayInWorldSpaceDir) * tri1C);

		//Make sure there are no divide-by-zeros
		if (ep2 != 0.0f)
			t = -(ep1 + tri1D) / (ep2);

		if (t > 0.0f)    //Make sure you don't pick objects behind the camera
		{
			//Get the point on the plane
			planeIntersectX = XMVectorGetX(pickRayInWorldSpacePos) + XMVectorGetX(pickRayInWorldSpaceDir) * t;
			planeIntersectY = XMVectorGetY(pickRayInWorldSpacePos) + XMVectorGetY(pickRayInWorldSpaceDir) * t;
			planeIntersectZ = XMVectorGetZ(pickRayInWorldSpacePos) + XMVectorGetZ(pickRayInWorldSpaceDir) * t;

			pointInPlane = XMVectorSet(planeIntersectX, planeIntersectY, planeIntersectZ, 0.0f);

			//Call function to check if point is in the triangle
			if (PointInTriangle(tri1V1, tri1V2, tri1V3, pointInPlane))
			{
				//Return the distance to the hit, so you can check all the other pickable objects in your scene
				//and choose whichever object is closest to the camera
				return t / 2.0f;
			}
		}
	}
	//return the max float value (near infinity) if an object was not picked
	return FLT_MAX;
}

bool PickingClass::PointInTriangle(XMVECTOR& triV1, XMVECTOR& triV2, XMVECTOR& triV3, XMVECTOR& point) {

	//To find out if the point is inside the triangle, we will check to see if the point
	//is on the correct side of each of the triangles edges.

	XMVECTOR cp1 = XMVector3Cross((triV3 - triV2), (point - triV2));
	XMVECTOR cp2 = XMVector3Cross((triV3 - triV2), (triV1 - triV2));
	if (XMVectorGetX(XMVector3Dot(cp1, cp2)) >= 0)
	{
		cp1 = XMVector3Cross((triV3 - triV1), (point - triV1));
		cp2 = XMVector3Cross((triV3 - triV1), (triV2 - triV1));
		if (XMVectorGetX(XMVector3Dot(cp1, cp2)) >= 0)
		{
			cp1 = XMVector3Cross((triV2 - triV1), (point - triV1));
			cp2 = XMVector3Cross((triV2 - triV1), (triV3 - triV1));
			if (XMVectorGetX(XMVector3Dot(cp1, cp2)) >= 0)
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	return false;
}
