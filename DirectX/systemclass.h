#pragma once
#include "graphicsclass.h"
#include "inputclass.h"
#include "soundclass.h"

static int ClientWidth = 0;
static int ClientHeight = 0;

class SystemClass {

public:
	
	SystemClass(HINSTANCE hInstance);
	bool Initialize();
	bool InitializeWindow();
	int messageloop();
	void CleanUp();


private:

	LPCTSTR WndClassName = L"DirectX_Engine_Framework";    //Define window class name
	HINSTANCE hInstance;
	HRESULT hr;
	HWND hwnd;

	GraphicsClass* m_Graphics;
	InputClass* m_Input;
	SoundClass* m_Sound;

	bool gameIsRun = false;

};

static LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);



