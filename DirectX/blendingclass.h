#pragma once
#include "d3d11class.h"

class BlendingClass {
public:
	BlendingClass(ID3D11Device* d3d11Device);
	void initBlending();
	void CleanUp();

public:
	ID3D11Device* d3d11Device;
	D3D11_RASTERIZER_DESC cmdesc;

	//Blending
	ID3D11BlendState* Transparency;
	ID3D11RasterizerState* CCWcullMode;
	ID3D11RasterizerState* CWcullMode;
};
