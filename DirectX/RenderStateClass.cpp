#include "renderstateclass.h"
RenderStateClass::RenderStateClass(ID3D11Device* d3d11DeviceM, ID3D11DeviceContext* d3d11DevConM) {
	d3d11Device = d3d11DeviceM;
	d3d11DevCon = d3d11DevConM;
}

void RenderStateClass::SetRenderState() {

	D3D11_RASTERIZER_DESC wfdesc;
	ZeroMemory(&wfdesc, sizeof(D3D11_RASTERIZER_DESC));
	//wfdesc.FillMode = D3D11_FILL_WIREFRAME;
	wfdesc.FillMode = D3D11_FILL_SOLID;
	wfdesc.CullMode = D3D11_CULL_NONE;
	d3d11Device->CreateRasterizerState(&wfdesc, &WireFrame);

	d3d11DevCon->RSSetState(WireFrame);
}

void RenderStateClass::CleanUp() {

	WireFrame->Release();
}