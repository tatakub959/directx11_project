#include "systemclass.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {

	SystemClass* System;
	bool result;

	System = new SystemClass(hInstance);
	result = System->Initialize();
	if (result)
	{
		System->messageloop();
	}

	System->CleanUp();
	delete System;
	System = 0;

	return 0;

}


