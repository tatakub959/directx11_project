#pragma once
#include "d3d11class.h"

class HeightMapClass {

public:

	//Square
	ID3D11Buffer* squareIndexBuffer;
	ID3D11Buffer* squareVertBuffer;

	//Heightmap
	int NumFaces = 0;
	int NumVertices = 0;

	XMMATRIX groundWorld;
	XMMATRIX Scale;
	XMMATRIX Translation;
	XMMATRIX RotationX;

	//Ground Collision
	std::vector<XMFLOAT3> collidableGeometryPositions;
	std::vector<DWORD> collidableGeometryIndices;


	//----------------------------------Struct---------------------------------
	struct HeightMapInfo {

		int terrainWidth;
		int terrainHeight;
		XMFLOAT3 *heightMap;

	};
	HeightMapInfo hminfo;

	struct Vertex
	{
		Vertex() {}
		Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, float tx, float ty, float tz) :
			pos(x, y, z), texCoord(u, v), normal(nx, ny, nz), tangent(tx, ty, tz) {}

		XMFLOAT3 pos;
		XMFLOAT2 texCoord;
		XMFLOAT3 normal;

		XMFLOAT3 tangent;
		XMFLOAT3 biTangent;
	};

public:

	bool HeightMapLoad(char* filename);
	void initHeightMap(ID3D11Device* d3d11Device, ID3D11DeviceContext* d3d11DevCon, ID3D10Blob* VS_Buffer);
	void CleanUp();

};