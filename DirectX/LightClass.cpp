#include "lightclass.h"

void LightClass::initLight(ID3D11Device* d3d11Device) {
	
	D3DX11CompileFromFile(L"Effects2.hlsl", 0 , 0 , "D2D_PS" , "ps_4_0" , 0 ,0 ,0 , &D2D_PS_Buffer , 0 ,0);
	d3d11Device->CreatePixelShader(D2D_PS_Buffer->GetBufferPointer(), D2D_PS_Buffer->GetBufferSize(), NULL, &D2D_PS);

	////Light information "Flashlight"-------------------------------------
	light.pos = XMFLOAT3(0.0f, 1.0f, 0.0f);
	light.dir = XMFLOAT3(0.0f, 0.0f, 1.0f);
	light.range = 1000.0f;
	light.cone = 30.0f;
	light.att = XMFLOAT3(0.5f, 0.001f, 0.001f);
	//light.ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	//light.ambient = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	light.diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);


	////Light information "Ghost"-------------------------------------
	lightGhost.pos = XMFLOAT3(0.0f, 0.0f, 0.0f);
	lightGhost.att = XMFLOAT3(0.1f, 0.1f, 0.1f);
	lightGhost.diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	//////Light information "Park"-------------------------------------
	//for (int i = 0; i < DecorLight_num; i++) {
	//	DecoratedLight.diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	//	DecoratedLight.att = XMFLOAT3(0.1f, 0.1f, 0.1f);

	//	if(i == 0) DecoratedLight.DecorPos[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	//	else if (i == 1) DecoratedLight.DecorPos[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	//	else if (i == 2) DecoratedLight.DecorPos[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	//	else if (i == 3) DecoratedLight.DecorPos[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	//	else if (i == 4) DecoratedLight.DecorPos[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	//	else if (i == 5) DecoratedLight.DecorPos[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	//	else if (i == 6) DecoratedLight.DecorPos[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	//	else if (i == 7) DecoratedLight.DecorPos[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	//	else if (i == 8) DecoratedLight.DecorPos[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);

	//}

	//Create the buffer to send to the cbuffer per frame in effect file
	D3D11_BUFFER_DESC cbbd;
	ZeroMemory(&cbbd, sizeof(D3D11_BUFFER_DESC));

	cbbd.Usage = D3D11_USAGE_DEFAULT;
	cbbd.ByteWidth = sizeof(cbPerFrame);
	cbbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbbd.CPUAccessFlags = 0;
	cbbd.MiscFlags = 0;

	d3d11Device->CreateBuffer(&cbbd, NULL, &cbPerFrameBuffer);

}

void LightClass::drawLight(ID3D11DeviceContext* d3d11DevCon) {

	constbuffPerFrame.light = light;
	constbuffPerFrame.lightGhost = lightGhost;
	//constbuffPerFrame.DecoratedLight = DecoratedLight;
	d3d11DevCon->UpdateSubresource(cbPerFrameBuffer, 0, NULL, &constbuffPerFrame, 0, 0);
	d3d11DevCon->PSSetConstantBuffers(0, 1, &cbPerFrameBuffer);

}

void LightClass::UpdateLight(XMVECTOR camPosition , XMVECTOR camTarget) {

	light.pos.x = XMVectorGetX(camPosition);
	light.pos.y = XMVectorGetY(camPosition);
	light.pos.z = XMVectorGetZ(camPosition);

	light.dir.x = XMVectorGetX(camTarget) - light.pos.x;
	light.dir.y = XMVectorGetY(camTarget) - light.pos.y;
	light.dir.z = XMVectorGetZ(camTarget) - light.pos.z;

}

void LightClass::UpdateLightGhost(XMMATRIX GhostWorld) {

	XMVECTOR lightVector = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);

	lightVector = XMVector3TransformCoord(lightVector, GhostWorld);

	lightGhost.pos.x = XMVectorGetX(lightVector);
	lightGhost.pos.y = XMVectorGetY(lightVector) + 10.0f;
	lightGhost.pos.z = XMVectorGetZ(lightVector);
}

void LightClass::CleanUp() {

	cbPerFrameBuffer->Release();
	D2D_PS->Release();
	D2D_PS_Buffer->Release();
}