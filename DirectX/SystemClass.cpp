#include "systemclass.h"

SystemClass::SystemClass(HINSTANCE hInstanceM) {
	hInstance = hInstanceM;
}

bool SystemClass::Initialize() {
	bool result;

	InitializeWindow();

	//********************************************Graphic**********************************************
	m_Graphics = new GraphicsClass(hwnd , ClientWidth , ClientHeight);
	if(FAILED(m_Graphics->InitializeDirect3d11App()))
	{
		MessageBox(0, L"Direct 3D initialize - Failed", L"Error!", MB_OK);
		return false;
	}
	if (FAILED(m_Graphics->InitScene()))
	{
		MessageBox(0, L"Scene initialize - Failed", L"Error!", MB_OK);
		return false;
	}

	//********************************************Input**********************************************
	m_Input = new InputClass(hInstance, hwnd);
	if (FAILED(m_Input->InitDirecInput()))
	{
		MessageBox(0, L"Input initialize - Failed", L"Error!", MB_OK);
		return false;
	}

	//********************************************Sound**********************************************
	m_Sound = new SoundClass();
	if (FAILED(m_Sound->Initialize(hwnd))) {
		MessageBox(0, L"Could not initialize Direct Sound.", L"Error", MB_OK);
		return false;
	}
	
	return true;

}

////----------------------------------------------InitializeWindow-------------------------------------------------
bool SystemClass::InitializeWindow() {
	int posX, posY;
	//Start creating the window//

	WNDCLASSEX wc;    //Create a new extended windows class
	DEVMODE dmScreenSettings;

	wc.cbSize = sizeof(WNDCLASSEX);    //Size of our windows class
	wc.style = CS_HREDRAW | CS_VREDRAW;    //class styles
	wc.lpfnWndProc = WndProc;    //Default windows procedure function
	wc.cbClsExtra = NULL;    //Extra bytes after our wc structure
	wc.cbWndExtra = NULL;    //Extra bytes after our windows instance
	wc.hInstance = hInstance;    //Instance to current application
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);    //Title bar Icon
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);    //Default mouse Icon
												 //wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 2);    //Window bg color
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;    //Name of the menu attached to our window
	wc.lpszClassName = WndClassName;    //Name of our windows class
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO); //Icon in your taskbar

	if (!RegisterClassEx(&wc))    //Register our windows class
	{
		//if registration failed, display error
		MessageBox(NULL, L"Error registering class",
			L"Error", MB_OK | MB_ICONERROR);
		return false;
	}

	// Determine the resolution of the clients desktop screen.


	if (FULL_SCREEN) {

		Width = GetSystemMetrics(SM_CXSCREEN);
		Height = GetSystemMetrics(SM_CYSCREEN);

		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = (unsigned long)Width;
		dmScreenSettings.dmPelsHeight = (unsigned long)Height;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;
	}
	else
	{
		posX = (GetSystemMetrics(SM_CXSCREEN) - Width) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - Height) / 2;
	}

	//Create our Extended Window

	hwnd = CreateWindowEx(
		NULL,    //Extended style
		WndClassName,    //Name of our windows class
		L"DirectX_Window",    //Name in the title bar of our window
		WS_OVERLAPPEDWINDOW,    //style of our window
		posX, posY,    //Top left - Right corner of window
		Width,    //Width of our window
		Height,    //Height of our window
		NULL,    //Handle to parent window
		NULL,    //Handle to a Menu
		hInstance,    //Specifies instance of current program
		NULL    //used for an MDI client window
	);

	if (!hwnd)    //Make sure our window has been created
	{
		//If not, display error
		MessageBox(NULL, L"Error creating window",
			L"Error", MB_OK | MB_ICONERROR);
		return false;
	}

	ShowWindow(hwnd, SW_SHOW);    //Shows our window
	UpdateWindow(hwnd);    //Its good to update our window

	return true;    //if there were no errors, return true
}

////---------------------------------------------------------------CleanUp-----------------------------------------------------------
void SystemClass::CleanUp() {

	m_Graphics->CleanUp();
	m_Input->CleanUp();
	m_Sound->CleanUp();

	delete m_Graphics;
	m_Graphics = 0;

	delete m_Input;
	m_Input = 0;

	delete m_Sound;
	m_Sound = 0;
}

////----------------------------------------------------messageloop (Game loop)--------------------------------------------------
int SystemClass::messageloop() {

	MSG msg;    //Create a new message structure
	ZeroMemory(&msg, sizeof(MSG));    //clear message structure to NULL

	while (true)    //while there is a message
	{

		//if there was a windows message
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)    //if the message was WM_QUIT
				break;    //Exit the message loop

			TranslateMessage(&msg);    //Translate the message

									   //Send the message to default windows procedure
			DispatchMessage(&msg);
		}

		else {    //run game 

			m_Graphics->m_Timer->frameCount++;
			if (m_Graphics->m_Timer->GetTime() > 1.0f) {

				m_Graphics->m_Timer->fps = m_Graphics->m_Timer->frameCount;
				m_Graphics->m_Timer->frameCount = 0;
				m_Graphics->m_Timer->StartTimer();

			}
			//Input Detection
			m_Input->DetectInput(m_Graphics->m_Timer->GetFrameTime(), m_Graphics->IsPlayerCollied , gameIsRun);

			//Update camera (Camera Movement)
			m_Graphics->m_Camera->MovingCamera(m_Input->moveLeftRight, m_Input->moveBackForward, m_Input->camYaw, m_Input->camPitch, m_Input->isJump );

			//Update Scene - keep update game components
			m_Graphics->UpdateScene(m_Input->LeftClick , m_Input->RightClick,m_Input->pickWhat, m_Input->cdMethod, m_Input->isWalking , m_Input->is_Forward , m_Input->is_Backward, m_Input->is_left, m_Input->is_Right);
		
			//DrawUI_Start - Set all things in initial stages
			if (m_Graphics->m_UI->buttons_Hit[1] == 0) {
				m_Graphics->DrawUI();
				m_Graphics->isInUImode = true;
				gameIsRun = false;
				m_Input->camYaw = 0.0f;
				m_Input->camPitch = 0.0f;
			}

			//DrawScene - Game started!
			if ((m_Graphics->m_UI->buttons_Hit[1] == 1 && m_Graphics->isDead == false) || (m_Graphics->m_UI->buttons_Hit[3] == 1 && m_Graphics->isDead == false)) {
				gameIsRun = true;

				m_Graphics->isInUImode = false;
				m_Graphics->isRestart = false;
				m_Graphics->DrawScene();

				m_Sound->SoundPlay(m_Input->isWalking, m_Input->isRunning, m_Graphics->m_Picking->isPick, m_Graphics->isSpawn, m_Graphics->isJumpScare, m_Graphics->HeartIsBeating);

			}

			//DrawUI_End - Player died , set all to initial
			if (m_Graphics->isDead == true) {
				gameIsRun = false;
				m_Graphics->isInUImode = true;
				m_Sound->StopSound();
				m_Graphics->DrawUI_EndGame();
			}
			
			//Restart - If player hit restart button at End UI
			if (m_Graphics->m_UI->buttons_Hit[3] == 1) {
				m_Graphics->isRestart = true;		
			}

			//Click Quit button
			if (m_Graphics->m_UI->buttons_Hit[2] == 1 || m_Graphics->m_UI->buttons_Hit[4] == 1)
				PostQuitMessage(0);
		}

	}

	return (int)msg.wParam;        //return the message
}

////---------------------------------------------WndProc------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)    //Check message
	{

	case WM_KEYDOWN:    //For a key down
						//if escape key was pressed, display popup box
		if (wParam == VK_ESCAPE ) {
			//if (MessageBox(0, L"Are you sure you want to exit?", L"Exit?", MB_YESNO | MB_ICONQUESTION) == IDYES)

				//Release the windows allocated memory  
				DestroyWindow(hwnd);
		}
		return 0;

	case WM_DESTROY:    //if x button in top right was pressed
		PostQuitMessage(0);
		return 0;

	case WM_SIZE:
		ClientWidth = LOWORD(lParam);
		ClientHeight = HIWORD(lParam);
		return 0;
	}
	//return the message for windows to handle it
	return DefWindowProc(hwnd, msg, wParam, lParam);
}