#pragma once
#include "d3d11class.h"
const int numGrass = 1500;

class LoadModelClass {

public:

	//Ground mesh variables--------------------------------------------------------------------------
	XMMATRIX meshWorld;
	ID3D11Buffer* meshVertBuff;
	ID3D11Buffer* meshIndexBuff;

	std::vector<XMFLOAT3> groundVertPosArray;
	std::vector<DWORD> groundVertIndexArray;

	int meshSubsets = 0;
	std::vector<int> meshSubsetIndexStart;
	std::vector<int> meshSubsetTexture;


	//Interactive Object mesh variables---------------------------------------------------------------------
	ID3D11Buffer* InteractiveObj_VertBuff;
	ID3D11Buffer* InteractiveObj_IndexBuff;
	std::vector<XMFLOAT3> InteractiveObj_VertPosArray;
	std::vector<DWORD> InteractiveObj_VertIndexArray;
	int InteractiveObj_Subsets = 0;
	std::vector<int> InteractiveObj_SubsetIndexStart;
	std::vector<int>InteractiveObj_SubsetTexture;
	XMMATRIX InteractiveObj_World[5];
	int* InteractiveObj_Hit = new int[5];
	int InteractiveObj_num = 5;

	//Box coll
	std::vector<XMFLOAT3> InteractiveObj_BoundingBoxVertPosArray;
	std::vector<DWORD> InteractiveObj_BoundingBoxVertIndexArray;
	XMVECTOR InteractiveObj_BoundingBoxMinVertex[5];
	XMVECTOR InteractiveObj_BoundingBoxMaxVertex[5];

	//Sphere coll
	float InteractiveObj_BoundingSphere = 0.0f;
	XMVECTOR InteractiveObj_CenterOffset;


	// Grass model -------------------------------------------------------------------------------------------------------------
	ID3D11Buffer* GrassInstanceBuff;
	ID3D11Buffer* GrassVertBuff;
	ID3D11Buffer* GrassIndexBuff;
	int GrassSubsets = 0;
	std::vector<int> GrassSubsetIndexStart;
	std::vector<int> GrassSubsetTexture;
	XMMATRIX GrassWorld;
	std::vector<XMFLOAT3> GrassVertPosArray;
	std::vector<DWORD> GrassVertIndexArray;


	// GhostPark mesh for collision**************************************************
	int GhostPark_Num = 9;
	ID3D11Buffer* GhostParkVertBuff[9];
	ID3D11Buffer* GhostParkIndexBuff[9];
	int GhostParkSubsets[9];
	std::vector<int> GhostParkSubsetIndexStart[9];
	std::vector<int> GhostParkSubsetTexture[9];
	XMMATRIX GhostParkWorld[9];
	std::vector<XMFLOAT3> GhostParkVertPosArray[9];
	std::vector<DWORD> GhostParkVertIndexArray[9];

	//Box coll
	std::vector<XMFLOAT3> GhostPark_BoundingBoxVertPosArray[9];
	std::vector<DWORD> GhostPark_BoundingBoxVertIndexArray[9];
	XMVECTOR GhostPark_BoundingBoxMinVertex[9];
	XMVECTOR GhostPark_BoundingBoxMaxVertex[9];
	//Sphere coll
	float GhostPark_BoundingSphere[9];
	XMVECTOR GhostPark_CenterOffset[9];

	// Ghost model -----------------------------------------------------------------------------------------------------------------
	ID3D11Buffer* GhostVertBuff;
	ID3D11Buffer* GhostIndexBuff;
	int GhostSubsets = 0;
	std::vector<int> GhostSubsetIndexStart;
	std::vector<int> GhostSubsetTexture;
	XMMATRIX GhostWorld;
	std::vector<XMFLOAT3> GhostVertPosArray;
	std::vector<DWORD> GhostVertIndexArray;

	// Tree model -------------------------------------------------------------------------------------------------------------
	ID3D11Buffer* TreeVertBuff;
	ID3D11Buffer* TreeIndexBuff;
	int TreeSubsets = 0;
	std::vector<int> TreeSubsetIndexStart;
	std::vector<int> TreeSubsetTexture;
	XMMATRIX TreeWorld[6];
	std::vector<XMFLOAT3> TreeVertPosArray;
	std::vector<DWORD> TreeVertIndexArray;
	int Tree_num = 6;

	//Fence Object mesh variables--------------------------------------------------------------------------------------------------
	ID3D11Buffer* Fence_VertBuff;
	ID3D11Buffer* Fence_IndexBuff;
	std::vector<XMFLOAT3> Fence_VertPosArray;
	std::vector<DWORD> Fence_VertIndexArray;
	int Fence_Subsets = 0;
	std::vector<int> Fence_SubsetIndexStart;
	std::vector<int>Fence_SubsetTexture;
	XMMATRIX Fence_World[160];
	int Fence_num = 160;

	//Box coll
	std::vector<XMFLOAT3> Fence_BoundingBoxVertPosArray;
	std::vector<DWORD> Fence_BoundingBoxVertIndexArray;
	XMVECTOR Fence_BoundingBoxMinVertex[160];
	XMVECTOR Fence_BoundingBoxMaxVertex[160];
	//Sphere coll
	float Fence_BoundingSphere = 0.0f;
	XMVECTOR Fence_CenterOffset;

	//MetalFence Object mesh variables--------------------------------------------------------------------------------------------------
	ID3D11Buffer* MetalFence_VertBuff;
	ID3D11Buffer* MetalFence_IndexBuff;
	std::vector<XMFLOAT3> MetalFence_VertPosArray;
	std::vector<DWORD> MetalFence_VertIndexArray;
	int MetalFence_Subsets = 0;
	std::vector<int> MetalFence_SubsetIndexStart;
	std::vector<int> MetalFence_SubsetTexture;
	XMMATRIX MetalFence_World[6];
	int MetalFence_num = 6;

	//Box coll
	std::vector<XMFLOAT3> MetalFence_BoundingBoxVertPosArray;
	std::vector<DWORD> MetalFence_BoundingBoxVertIndexArray;
	XMVECTOR MetalFence_BoundingBoxMinVertex[6];
	XMVECTOR MetalFence_BoundingBoxMaxVertex[6];
	//Sphere coll
	float MetalFence_BoundingSphere = 0.0f;
	XMVECTOR MetalFence_CenterOffset;

	
	//Mesh and tex name
	std::vector<ID3D11ShaderResourceView*> meshSRV;
	std::vector<std::wstring> textureNameArray;

	XMMATRIX Rotation;
	XMMATRIX Rotation2;
	XMMATRIX Scale;
	XMMATRIX Translation;

	float MoveToPlayer = 0;

	struct SurfaceMaterial {
		std::wstring matName;
		XMFLOAT4 difColor;
		int texArrayIndex;

		int normMapTexArrayIndex;
		bool hasNormMap;

		bool hasTexture;
		bool transparent;
	};
	std::vector<SurfaceMaterial> material;

	struct Vertex
	{
		Vertex() {}
		Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, float tx, float ty, float tz) :
			pos(x, y, z), texCoord(u, v), normal(nx, ny, nz), tangent(tx, ty, tz) {}

		XMFLOAT3 pos;
		XMFLOAT2 texCoord;
		XMFLOAT3 normal;

		XMFLOAT3 tangent;
		XMFLOAT3 biTangent;
	};

public:

	bool LoadObjModel(std::wstring filename, ID3D11Buffer** vertBuff, ID3D11Buffer** indexBuff, std::vector<int>& subsetIndexStart,
		std::vector<int>& subsetMaterialArray, std::vector<SurfaceMaterial>& material, int& subsetCount, bool isRHCoordSys, bool computeNormals, std::vector<XMFLOAT3>& VertPosArray,
		std::vector<DWORD>& VertIndexArray, IDXGISwapChain* SwapChain, ID3D11Device* d3d11Device);

	void initGroundAndPark();
	void initInteractiveObj();
	void initFence();
	void initMetalFence();
	void CreateBoundingVolumes(std::vector<XMFLOAT3> &vertPosArray, std::vector<XMFLOAT3>& boundingBoxVerts, std::vector<DWORD>& boundingBoxIndex, float &boundingSphere, XMVECTOR &objectCenterOffset);

	bool BoundingSphereCollision(float firstObjBoundingSphere,
		XMVECTOR firstObjCenterOffset,
		XMMATRIX& firstObjWorldSpace,
		float secondObjBoundingSphere,
		XMVECTOR secondObjCenterOffset,
		XMMATRIX& secondObjWorldSpace);

	bool BoundingBoxCollision(XMVECTOR& firstObjBoundingBoxMinVertex,
		XMVECTOR& firstObjBoundingBoxMaxVertex,
		XMVECTOR& secondObjBoundingBoxMinVertex,
		XMVECTOR& secondObjBoundingBoxMaxVertex);

	void CalculateAABB(std::vector<XMFLOAT3> boundingBoxVerts,
		XMMATRIX& worldSpace,
		XMVECTOR& boundingBoxMin,
		XMVECTOR& boundingBoxMax);

	void CalculateAABB_Player(std::vector<XMFLOAT3> boundingBoxVerts,
		XMVECTOR& worldSpace,
		XMVECTOR& boundingBoxMin,
		XMVECTOR& boundingBoxMax);

	void SpawnGhost(XMVECTOR playerPosition , bool isSpwan);

	
	void CleanUp();

};