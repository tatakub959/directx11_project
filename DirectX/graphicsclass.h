#pragma once
#include "cameraclass.h"
//#include "modelclass.h"
#include "blendingclass.h"
#include "timerclass.h"
#include "lightclass.h"
#include "skyboxclass.h"
#include "heightmapclass.h"
#include "loadmodelclass.h"
#include "pickingclass.h"
#include "uiclass.h"

static int Width = 1920;
static int Height = 1080;
const bool FULL_SCREEN = false;

class GraphicsClass{

public:

	GraphicsClass::GraphicsClass(HWND hwnd, int ClientWidth, int ClientHeight);
	bool InitializeDirect3d11App();

	bool InitD2D_D3D101_DWrite(IDXGIAdapter1 *Adapter);
	void InitD2DScreenTexture();
	void RenderText(std::wstring text, ID3D11ShaderResourceView *d2dTexture, int inInt);

	bool InitScene();

	void UpdateScene(bool LeftClick, bool RightClick, int pickWhat, int cdMethod , bool isWaling , bool w , bool s , bool a , bool d);
	void DrawScene();
	void DrawSky(UINT stride, UINT offset);
	void DrawUI();
	void DrawUI_EndGame();

	void DrawLoadedModel(XMMATRIX meshWorld, ID3D11Buffer* meshVertBuff, ID3D11Buffer* meshIndexBuff, int meshSubsets, std::vector<int> meshSubsetIndexStart,
		std::vector<int> meshSubsetTexture, UINT stride, UINT offset, bool isInstance , bool isGrass);

	void DrawLoadedInstance(bool isDrawInUI);

	void CleanUp();


public:

	HWND hwnd;

	CameraClass* m_Camera;
	//ModelClass* m_Model;
	BlendingClass* m_Blending;
	TimerClass* m_Timer;
	LightClass* m_Light;
	SkyboxClass* m_Sky;
	LoadModelClass* m_LoadModel;
	//HeightMapClass* m_HeightMap;
	//GroundCollisionClass* m_GroundCollision;
	PickingClass* m_Picking;
	UIclass* m_UI;

	//SetD3D11
	IDXGISwapChain* SwapChain;
	ID3D11Device* d3d11Device;
	ID3D11DeviceContext* d3d11DevCon;
	ID3D11RenderTargetView* renderTargetView;

	//Shader
	ID3D11VertexShader* VS;
	ID3D11PixelShader* PS;
	ID3D10Blob* VS_Buffer;
	ID3D10Blob* PS_Buffer;
	ID3D11InputLayout* vertLayout;
	ID3D11InputLayout* GrassVertLayout;

	//Texture
	ID3D11ShaderResourceView* groundTexture;
	ID3D11SamplerState* CubesTexSamplerState;
	//UI
	//ID3D11ShaderResourceView* startBGTexture;
	ID3D11ShaderResourceView* titleTexture;
	ID3D11ShaderResourceView* playTexture;
	ID3D11ShaderResourceView* quitTexture;

	ID3D11ShaderResourceView* EndBGTexture;
	//ID3D11ShaderResourceView* EndtitleTexture;
	ID3D11ShaderResourceView* RestartTexture;


	//Depth
	ID3D11DepthStencilView* depthStencilView;
	ID3D11Texture2D* depthStencilBuffer;

	//Text
	ID3D10Device1 *d3d101Device;
	IDXGIKeyedMutex *keyedMutex11;
	IDXGIKeyedMutex *keyedMutex10;
	ID2D1RenderTarget *D2DRenderTarget;
	ID2D1SolidColorBrush *Brush;
	ID3D11Texture2D *BackBuffer11;
	ID3D11Texture2D *sharedTex11;
	ID3D11Buffer *d2dVertBuffer;
	ID3D11Buffer *d2dIndexBuffer;
	ID3D11ShaderResourceView *d2dTexture;
	ID3D11ShaderResourceView *d2dTexture_crosshair;
	ID3D11ShaderResourceView *d2dTexture_blood;
	ID3D11ShaderResourceView *d2dTexture_Scare;
	IDWriteFactory *DWriteFactory;
	IDWriteTextFormat *TextFormat;
	std::wstring printText;

	int ClientWidth;
	int ClientHeight;

	int pickOptions;
	int collisionMethods;
	
	int numTreesToDraw;

	float RandomSpawnTime;
	float CountDownToDie;
	float AddTimeToSpwan;
	bool isSpawn = false;
	bool HeartIsBeating = false;
	bool isJumpScare = false;
	bool isDead = false;
	bool FlashLightOnOff;

	XMFLOAT3 InstPos[numGrass];
	XMVECTOR tempInstPos;

	//Player collision
	bool IsPlayerCollied[4];

	bool is_W;
	bool is_S;
	bool is_A;
	bool is_D;

	bool isInUImode;
	bool isRestart;

	struct Vertex
	{
		Vertex() {}
		Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, float tx, float ty, float tz) :
			pos(x, y, z), texCoord(u, v), normal(nx, ny, nz), tangent(tx, ty, tz) {}

		XMFLOAT3 pos;
		XMFLOAT2 texCoord;
		XMFLOAT3 normal;

		XMFLOAT3 tangent;
		XMFLOAT3 biTangent;
	};

	//Instance
	struct InstanceData
	{
		XMFLOAT3 pos;
	};


	D3D11_INPUT_ELEMENT_DESC layout[5] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",     0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		// Data from the instance buffer
		{ "INSTANCEPOS", 0, DXGI_FORMAT_R32G32B32_FLOAT,    1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 }

	};
	UINT numElements = ARRAYSIZE(layout);

	D3D11_INPUT_ELEMENT_DESC GrassLayout[5] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",     0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },

		{ "INSTANCEPOS", 0, DXGI_FORMAT_R32G32B32_FLOAT,    1, 0, D3D11_INPUT_PER_INSTANCE_DATA, numGrassPerGroup }
	};
	UINT numGrassElements = ARRAYSIZE(GrassLayout);

};