#pragma once
#include "d3d11class.h"

class TimerClass {
public:

	void StartTimer();
	double GetTime();
	double GetFrameTime();

public:

	double countsPerSecond = 0.0;
	 __int64 CounterStart = 0;

	int frameCount = 0;
	int fps = 0;

	__int64 frameTimeOld = 0;
	double frameTime;

};