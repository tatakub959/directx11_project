#pragma once
#include "d3d11class.h"
//#include "groundcollisionclass.h"
const int numGrassPerGroup = 10;
class CameraClass{

public:
	CameraClass(ID3D11Device* d3d11DeviceMain, ID3D11DeviceContext* d3d11DevConMain);
	void InitCamera(int Width, int Height /*, std::vector<XMFLOAT3> collidableGeometryPositionsM, std::vector<DWORD> collidableGeometryIndicesM*/ );
	void DrawCamera(XMMATRIX cubeWorld, ID3D11ShaderResourceView* CubesTexture, ID3D11SamplerState* CubesTexSamplerState , ID3D11RasterizerState* CWcullMode , UINT index, BOOL hasTexture, BOOL hasNormMap);
	void MovingCamera(float moveLeftRight, float moveBackForward , float camYaw, float camPitch , bool isJump	);

	void CleanUp();

public:

	ID3D11Device* d3d11Device;
	ID3D11DeviceContext* d3d11DevCon;

	//Camera
	ID3D11Buffer* cbPerObjectBuffer;
	XMMATRIX WVP;
	XMMATRIX World;
	XMMATRIX camProjection;

	XMMATRIX camView;
	XMVECTOR camPosition;
	XMVECTOR camTarget;
	XMVECTOR camUp;

	ID3D11Buffer* cbPerInstanceBuffer;
	XMMATRIX Scale;
	XMMATRIX Translation;

	//Ground Collision-------------------------------------
	//GroundCollisionClass* m_GroundCollision;
	//std::vector<XMFLOAT3> collidableGeometryPositions;
	//std::vector<DWORD> collidableGeometryIndices;

	//AABB
	XMVECTOR camBoundingBoxMinVertex;
	XMVECTOR camBoundingBoxMaxVertex;

	//Sphere coll
	float cam_BoundingSphere = 0.0f;
	XMVECTOR cam_CenterOffset;

	//Create effects constant buffer's structure//
	struct cbPerObject
	{
		XMMATRIX  WVP;
		XMMATRIX World;

		XMFLOAT4 difColor;
		BOOL hasTexture;
		BOOL hasNormMap;

		//Instance
		BOOL isInstance;
		BOOL isGrass;
	};
	cbPerObject cbPerObj;
	
	//Constant buffer's scene
	struct cbPerScene 
	{
		XMMATRIX Grass[numGrassPerGroup];
	};
	cbPerScene cbPerInst;

	//Moving camera
	XMVECTOR DefaultForward = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
	XMVECTOR DefaultRight = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);

	XMVECTOR camForward = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
	XMVECTOR camRight = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);

	XMMATRIX camRotationMatrix;
	XMMATRIX RotateYTempMatrix;

	bool is_Forward;
	bool is_Right;

	bool Collide_Forward;

};
