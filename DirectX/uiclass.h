#pragma once
#include "d3d11class.h"
class UIclass {
public:
	//0 = titel game , 1 = start , 2 = quit , 3 = restart game , 4 = quit in end

	XMMATRIX buttons[5];
	int* buttons_Hit = new int[5];
	int buttons_num = 5;
	XMMATRIX TitleEnd;

	ID3D11Buffer *quadVertBuffer;
	ID3D11Buffer *quadIndexBuffer;

	std::vector<XMFLOAT3> button_VertPosArray;
	std::vector<DWORD> button_VertIndexArray;

	XMMATRIX Scale;
	XMMATRIX Translation;

	//Box coll
	std::vector<XMFLOAT3> button_BoundingBoxVertPosArray;
	std::vector<DWORD> button_BoundingBoxVertIndexArray;
	XMVECTOR button_BoundingBoxMinVertex[5];
	XMVECTOR button_BoundingBoxMaxVertex[5];
	//Sphere coll
	float button_BoundingSphere = 0.0f;
	XMVECTOR button_CenterOffset;

	struct Vertex
	{
		Vertex() {}
		Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, float tx, float ty, float tz) :
			pos(x, y, z), texCoord(u, v), normal(nx, ny, nz), tangent(tx, ty, tz) {}

		XMFLOAT3 pos;
		XMFLOAT2 texCoord;
		XMFLOAT3 normal;

		XMFLOAT3 tangent;
		XMFLOAT3 biTangent;
	};

public:

	void initUI(ID3D11Device* d3d11Device, ID3D11DeviceContext* d3d11DevCon, ID3D10Blob* VS_Buffer);
	void UpdateUI();
	void UpdateUIEnding(XMVECTOR camPosition);
	void CleanUp();
};