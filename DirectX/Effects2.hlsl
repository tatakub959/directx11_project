#define NUM_GRASS_PER_GROUP 10
struct Light
{
	//----------------------------------Directlight effect-------------------------------------
	//float3 dir;
	//float4 ambient;
	//float4 diffuse;

	//----------------------------------Spotlight effect-------------------------------------
	float3 pos;
	float  range;
	float3 dir;
	float cone;
	float3 att;
	float4 ambient;
	float4 diffuse;

	//----------------------------------Pointlight effect-------------------------------------
	//float3 dir;
	//float3 pos;
	//float  range;
	//float3 att;
	//float4 ambient;
	//float4 diffuse;

};

cbuffer cbPerFrame
{
	Light light;
	Light lightGhost;
	Light ightPark;
};

cbuffer cbPerObject
{
	float4x4 WVP;
	float4x4 World;

	//Load model
	float4 difColor;
	bool hasTexture;
	bool hasNormMap;

	bool isInstance;
	bool isGrass;
};

cbuffer cbPerScene
{
	float4x4 Grass[NUM_GRASS_PER_GROUP];
};

Texture2D ObjTexture;
Texture2D ObjNormMap;
SamplerState ObjSamplerState;
TextureCube SkyMap;

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float4 worldPos : POSITION;
	float2 TexCoord : TEXCOORD;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
};

struct SKYMAP_VS_OUTPUT    //output structure for skymap vertex shader
{
	float4 Pos : SV_POSITION;
	float3 texCoord : TEXCOORD;
};

VS_OUTPUT VS(float4 inPos : POSITION, float2 inTexCoord : TEXCOORD, float3 normal : NORMAL, float3 tangent : TANGENT, float3 instancePos : INSTANCEPOS, uint instanceID : SV_InstanceID)
{
	VS_OUTPUT output;

	if (isInstance)
	{
		if (isGrass)
		{
			uint currGrass = (instanceID / NUM_GRASS_PER_GROUP);
			uint currGrassInGroup = instanceID - (currGrass * NUM_GRASS_PER_GROUP);
			inPos = mul(inPos, Grass[currGrassInGroup]);
		}

		// set position using instance data
		inPos += float4(instancePos, 0.0f);
	}

	output.Pos = mul(inPos, WVP);

	output.worldPos = mul(inPos, World);

	output.normal = mul(normal, World);

	output.tangent = mul(tangent, World);

	output.TexCoord = inTexCoord;

	return output;
}

SKYMAP_VS_OUTPUT SKYMAP_VS(float3 inPos : POSITION, float2 inTexCoord : TEXCOORD, float3 normal : NORMAL, float3 tangent : TANGENT)
{
	SKYMAP_VS_OUTPUT output = (SKYMAP_VS_OUTPUT)0;

	output.Pos = mul(float4(inPos, 1.0f), WVP).xyww;

	output.texCoord = inPos;

	return output;
}

float4 PS(VS_OUTPUT input) : SV_TARGET
{
	//----------------------------------Directlight effect-------------------------------------
	//input.normal = normalize(input.normal);

	//float4 diffuse = ObjTexture.Sample(ObjSamplerState, input.TexCoord);
	//clip(diffuse.a - 0.5);
	//float3 finalColor;

	//finalColor = diffuse * light.ambient;
	//finalColor += saturate(dot(light.dir, input.normal) * light.diffuse * diffuse);

	//return float4(finalColor, diffuse.a);

	//----------------------------------Spotlight or Pointlight effect-------------------------------------
	input.normal = normalize(input.normal);

	float4 diffuse;

	//float4 diffuse = ObjTexture.Sample(ObjSamplerState, input.TexCoord);

	if (hasTexture == true)
		diffuse = ObjTexture.Sample(ObjSamplerState, input.TexCoord);
	else
		diffuse = difColor;

	clip(diffuse.a - 0.6);

	if (hasNormMap == true)
	{

		float4 normalMap = ObjNormMap.Sample(ObjSamplerState, input.TexCoord);


		normalMap = (2.0f*normalMap) - 1.0f;


		input.tangent = normalize(input.tangent - dot(input.tangent, input.normal)*input.normal);

		float3 biTangent = cross(input.normal, input.tangent);


		float3x3 texSpace = float3x3(input.tangent, biTangent, input.normal);


		input.normal = normalize(mul(normalMap, texSpace));
	}
	float3 AllColors;

	//Flashlight************************************************************
	float3 finalColor = float3(0.0f, 0.0f, 0.0f);
	float3 lightToPixelVec = light.pos - input.worldPos;
	float d = length(lightToPixelVec);

	lightToPixelVec /= d;
	finalColor += diffuse * light.diffuse;
	finalColor /= (light.att[0] + (light.att[1] * d)) + (light.att[2] * (d*d));
	finalColor *= pow(max(dot(-lightToPixelVec, light.dir), 0.0f), light.cone);

	float3 finalAmbient = diffuse * light.ambient;

	if (d > light.range)
		return float4(finalAmbient, diffuse.a);

	//Ghost Light***********************************************************
	float3 finalColorGhost = float3(0.0f, 0.0f, 0.0f);
	float3 lightGhostToPixelVec = lightGhost.pos - input.worldPos;
	float d_Ghost = length(lightGhostToPixelVec);

	lightGhostToPixelVec /= d_Ghost;
	finalColorGhost += diffuse * lightGhost.diffuse;
	finalColorGhost /= (lightGhost.att[0] + (lightGhost.att[1] * d_Ghost)) + (lightGhost.att[2] * (d_Ghost*d_Ghost));

	//Park Lights***********************************************************
	//float3 finalColorPark = float3(0.0f, 0.0f, 0.0f);
	//float3 lightParkToPixelVec = lightGhost.DecorPos[0] - input.worldPos;
	//float d_Park = length(lightParkToPixelVec);

	//lightParkToPixelVec /= d_Park;
	//finalColorPark += diffuse * ightPark.diffuse;
	//finalColorPark /= (ightPark.att[0] + (ightPark.att[1] * d_Park)) + (ightPark.att[2] * (d_Park*d_Park));

	//Combine all lights
	AllColors = saturate(finalColor + finalColorGhost  + finalAmbient) ;

	return float4(AllColors, diffuse.a);

}



float4 SKYMAP_PS(SKYMAP_VS_OUTPUT input) : SV_Target
{
	return SkyMap.Sample(ObjSamplerState, input.texCoord);
}

float4 D2D_PS(VS_OUTPUT input) : SV_TARGET
{
	float4 diffuse = ObjTexture.Sample(ObjSamplerState, input.TexCoord);
	clip(diffuse.a - 0.6);
	return diffuse;
}