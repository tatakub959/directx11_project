#pragma once
#include "d3d11class.h"

class SkyboxClass {

public:
	SkyboxClass(ID3D11Device* d3d11Device);
	void CreateSphere(int LatLines, int LongLines);
	void initSky(D3D11_RASTERIZER_DESC cmdesc);
	void UpdateSky(XMVECTOR camPosition);
	void CleanUp();

public:
	ID3D11Device* d3d11Device;
	ID3D11DeviceContext* d3d11DevCon;

	ID3D11Buffer* sphereIndexBuffer;
	ID3D11Buffer* sphereVertBuffer;

	ID3D11VertexShader* SKYMAP_VS;
	ID3D11PixelShader* SKYMAP_PS;
	ID3D10Blob* SKYMAP_VS_Buffer;
	ID3D10Blob* SKYMAP_PS_Buffer;

	ID3D11ShaderResourceView* smrv;

	ID3D11DepthStencilState* DSLessEqual;
	ID3D11RasterizerState* RSCullNone;

	int NumSphereVertices;
	int NumSphereFaces;

	XMMATRIX sphereWorld;

	XMMATRIX Rotationx;
	XMMATRIX Rotationy;
	XMMATRIX Rotationz;

	XMMATRIX Scale;
	XMMATRIX Translation;

	struct Vertex
	{
		Vertex() {}
		Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, float tx, float ty, float tz) :
			pos(x, y, z), texCoord(u, v), normal(nx, ny, nz), tangent(tx, ty, tz) {}

		XMFLOAT3 pos;
		XMFLOAT2 texCoord;
		XMFLOAT3 normal;

		XMFLOAT3 tangent;
		XMFLOAT3 biTangent;
	};

};
