#include "graphicsclass.h"

GraphicsClass::GraphicsClass(HWND hwndM, int ClientWidthM, int ClientHeightM) {
	hwnd = hwndM;
	m_Timer = new TimerClass;
	m_Light = new LightClass;
	//m_Model = new ModelClass();
	//m_HeightMap = new HeightMapClass();
	m_LoadModel = new LoadModelClass();
	m_UI = new UIclass();

	ClientWidth = ClientWidthM;
	ClientHeight = ClientHeightM;

}

//-----------------------------------------------------Initialize Direct3d11App------------------------------------------------------------------------------------------------------
bool GraphicsClass::InitializeDirect3d11App() {

	HRESULT hr;

	//Describe our SwapChain Buffer
	DXGI_MODE_DESC bufferDesc;

	ZeroMemory(&bufferDesc, sizeof(DXGI_MODE_DESC));

	bufferDesc.Width = Width;
	bufferDesc.Height = Height;
	bufferDesc.RefreshRate.Numerator = 60;
	bufferDesc.RefreshRate.Denominator = 1;
	bufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	//Describe our SwapChain
	DXGI_SWAP_CHAIN_DESC swapChainDesc;

	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapChainDesc.BufferDesc = bufferDesc;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.Windowed = true;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	// Create DXGI factory to enumerate adapters
	IDXGIFactory1 *DXGIFactory;

	hr = CreateDXGIFactory1(__uuidof(IDXGIFactory1), (void**)&DXGIFactory);

	// Use the first adapter    
	IDXGIAdapter1 *Adapter;

	hr = DXGIFactory->EnumAdapters1(0, &Adapter);

	DXGIFactory->Release();

	//Create our Direct3D 11 Device and SwapChain
	hr = D3D11CreateDeviceAndSwapChain(Adapter, D3D_DRIVER_TYPE_UNKNOWN, NULL, D3D11_CREATE_DEVICE_DEBUG | D3D11_CREATE_DEVICE_BGRA_SUPPORT,
		NULL, NULL, D3D11_SDK_VERSION, &swapChainDesc, &SwapChain, &d3d11Device, NULL, &d3d11DevCon);

	//Initialize Direct2D, Direct3D 10.1, DirectWrite
	InitD2D_D3D101_DWrite(Adapter);

	//Release the Adapter interface
	Adapter->Release();

	//Create our BackBuffer and Render Target
	hr = SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&BackBuffer11);
	hr = d3d11Device->CreateRenderTargetView(BackBuffer11, NULL, &renderTargetView);

	//Describe our Depth/Stencil Buffer
	D3D11_TEXTURE2D_DESC depthStencilDesc;

	depthStencilDesc.Width = Width;
	depthStencilDesc.Height = Height;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	//Create the Depth/Stencil View
	d3d11Device->CreateTexture2D(&depthStencilDesc, NULL, &depthStencilBuffer);
	d3d11Device->CreateDepthStencilView(depthStencilBuffer, NULL, &depthStencilView);

	return true;
}

//---------------------------------------------------------------InitD2D_D3D101_Dwrite---------------------------------------------------------------------------------
bool GraphicsClass::InitD2D_D3D101_DWrite(IDXGIAdapter1 *Adapter) {

	HRESULT hr;
	//Create our Direc3D 10.1 Device///////////////////////////////////////////////////////////////////////////////////////
	hr = D3D10CreateDevice1(Adapter, D3D10_DRIVER_TYPE_HARDWARE, NULL, D3D10_CREATE_DEVICE_DEBUG | D3D10_CREATE_DEVICE_BGRA_SUPPORT,
		D3D10_FEATURE_LEVEL_9_3, D3D10_1_SDK_VERSION, &d3d101Device);

	//Create Shared Texture that Direct3D 10.1 will render on
	D3D11_TEXTURE2D_DESC sharedTexDesc;

	ZeroMemory(&sharedTexDesc, sizeof(sharedTexDesc));

	sharedTexDesc.Width = Width;
	sharedTexDesc.Height = Height;
	sharedTexDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	sharedTexDesc.MipLevels = 1;
	sharedTexDesc.ArraySize = 1;
	sharedTexDesc.SampleDesc.Count = 1;
	sharedTexDesc.Usage = D3D11_USAGE_DEFAULT;
	sharedTexDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
	sharedTexDesc.MiscFlags = D3D11_RESOURCE_MISC_SHARED_KEYEDMUTEX;

	hr = d3d11Device->CreateTexture2D(&sharedTexDesc, NULL, &sharedTex11);

	// Get the keyed mutex for the shared texture (for D3D11)
	hr = sharedTex11->QueryInterface(__uuidof(IDXGIKeyedMutex), (void**)&keyedMutex11);

	// Get the shared handle needed to open the shared texture in D3D10.1
	IDXGIResource *sharedResource10;
	HANDLE sharedHandle10;

	hr = sharedTex11->QueryInterface(__uuidof(IDXGIResource), (void**)&sharedResource10);

	hr = sharedResource10->GetSharedHandle(&sharedHandle10);

	sharedResource10->Release();

	// Open the surface for the shared texture in D3D10.1
	IDXGISurface1 *sharedSurface10;

	hr = d3d101Device->OpenSharedResource(sharedHandle10, __uuidof(IDXGISurface1), (void**)(&sharedSurface10));

	hr = sharedSurface10->QueryInterface(__uuidof(IDXGIKeyedMutex), (void**)&keyedMutex10);

	// Create D2D factory
	ID2D1Factory *D2DFactory;
	hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(ID2D1Factory), (void**)&D2DFactory);

	D2D1_RENDER_TARGET_PROPERTIES renderTargetProperties;

	ZeroMemory(&renderTargetProperties, sizeof(renderTargetProperties));

	renderTargetProperties.type = D2D1_RENDER_TARGET_TYPE_HARDWARE;
	renderTargetProperties.pixelFormat = D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED);

	hr = D2DFactory->CreateDxgiSurfaceRenderTarget(sharedSurface10, &renderTargetProperties, &D2DRenderTarget);

	sharedSurface10->Release();
	D2DFactory->Release();

	// Create a solid color brush to draw something with        
	hr = D2DRenderTarget->CreateSolidColorBrush(D2D1::ColorF(1.0f, 1.0f, 0.0f, 1.0f), &Brush);

	//DirectWrite
	hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory),
		reinterpret_cast<IUnknown**>(&DWriteFactory));

	hr = DWriteFactory->CreateTextFormat(
		L"Script",
		NULL,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		24.0f,
		L"en-us",
		&TextFormat
	);

	hr = TextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
	hr = TextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);

	d3d101Device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);
	return true;
}

//---------------------------------------------------------InitD2DScreenTexture-------------------------------------------------------------------------------------------------------
void GraphicsClass::InitD2DScreenTexture()
{
	//Create the vertex buffer
	Vertex v[] =
	{
		// Front Face
		Vertex(-1.0f, -1.0f, -1.0f, 0.0f, 1.0f,-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f,-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
	};

	DWORD indices[] = {
		// Front Face
		0,  1,  2,
		0,  2,  3,
	};

	D3D11_BUFFER_DESC indexBufferDesc;
	ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(DWORD) * 2 * 3;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA iinitData;

	iinitData.pSysMem = indices;
	d3d11Device->CreateBuffer(&indexBufferDesc, &iinitData, &d2dIndexBuffer);


	D3D11_BUFFER_DESC vertexBufferDesc;
	ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(Vertex) * 4;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;

	ZeroMemory(&vertexBufferData, sizeof(vertexBufferData));
	vertexBufferData.pSysMem = v;
	d3d11Device->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &d2dVertBuffer);
	
	//2D Textures
	d3d11Device->CreateShaderResourceView(sharedTex11, NULL, &d2dTexture);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"Assets\\Textures\\Crosshair_W.png", NULL, NULL, &d2dTexture_crosshair, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"Assets\\Textures\\blood_PNG6093.png", NULL, NULL, &d2dTexture_blood, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"Assets\\Textures\\Kuntilanak_jumpscare.jpg", NULL, NULL, &d2dTexture_Scare, NULL);
}

//---------------------------------------------------------InitScene------------------------------------------------------------------------------------------------------------------
bool GraphicsClass::InitScene()
{
	//Load model**********************************************************

	//Ground
	if (!m_LoadModel->LoadObjModel(L"Assets\\Models\\ground.obj", &m_LoadModel->meshVertBuff, &m_LoadModel->meshIndexBuff, m_LoadModel->meshSubsetIndexStart,
		m_LoadModel->meshSubsetTexture, m_LoadModel->material, m_LoadModel->meshSubsets, true, true, m_LoadModel->groundVertPosArray, m_LoadModel->groundVertIndexArray, SwapChain, d3d11Device))
		return false;

	//Interactive Objs
	if (!m_LoadModel->LoadObjModel(L"Assets\\Models\\note_A4_OBJ(3ds_Max_2013__Default_Scanline).obj", &m_LoadModel->InteractiveObj_VertBuff, &m_LoadModel->InteractiveObj_IndexBuff, m_LoadModel->InteractiveObj_SubsetIndexStart, m_LoadModel->InteractiveObj_SubsetTexture,
		m_LoadModel->material, m_LoadModel->InteractiveObj_Subsets, true, true, m_LoadModel->InteractiveObj_VertPosArray, m_LoadModel->InteractiveObj_VertIndexArray, SwapChain, d3d11Device))
		return false;

	//Grass model
	if (!m_LoadModel->LoadObjModel(L"Assets\\Models\\Grass1.obj", &m_LoadModel->GrassVertBuff, &m_LoadModel->GrassIndexBuff, m_LoadModel->GrassSubsetIndexStart, m_LoadModel->GrassSubsetTexture, m_LoadModel->material,
		m_LoadModel->GrassSubsets, true, true, m_LoadModel->GrassVertPosArray, m_LoadModel->GrassVertIndexArray, SwapChain, d3d11Device))
		return false;

	//Tree decorated model
	if (!m_LoadModel->LoadObjModel(L"Assets\\Models\\GhostPark_Tree.obj", &m_LoadModel->TreeVertBuff, &m_LoadModel->TreeIndexBuff, m_LoadModel->TreeSubsetIndexStart, m_LoadModel->TreeSubsetTexture, m_LoadModel->material,
		m_LoadModel->TreeSubsets, true, true, m_LoadModel->TreeVertPosArray, m_LoadModel->TreeVertIndexArray, SwapChain, d3d11Device))
		return false;

	//Ghostpark each model
	for (int i = 0; i < m_LoadModel->GhostPark_Num; i++) {
		std::wstring GhostParkFileName;
		if(i == 0) GhostParkFileName = L"Assets\\Models\\GhostParkModel1_Big_Wheel_With_Seat_NoCTRL.obj";
		else if (i == 1) GhostParkFileName = L"Assets\\Models\\GhostParkModel2_Drop_Tower.obj";
		else if (i == 2) GhostParkFileName = L"Assets\\Models\\GhostParkModel3_Flat_rotator.obj";
		else if (i == 3) GhostParkFileName = L"Assets\\Models\\GhostParkModel4_Prize_Booth.obj";
		else if (i == 4) GhostParkFileName = L"Assets\\Models\\GhostParkModel5_Prize_Booth2.obj";
		else if (i == 5) GhostParkFileName = L"Assets\\Models\\GhostParkModel6_Test_your_strength.obj";
		else if (i == 6) GhostParkFileName = L"Assets\\Models\\GhostParkModel7_Twister.obj";
		else if (i == 7) GhostParkFileName = L"Assets\\Models\\GhostParkModel4_Prize_Booth.obj";
		else if (i == 8) GhostParkFileName = L"Assets\\Models\\GhostParkModel8_Tailer.obj";
	
		if (!m_LoadModel->LoadObjModel(GhostParkFileName, &m_LoadModel->GhostParkVertBuff[i], &m_LoadModel->GhostParkIndexBuff[i], m_LoadModel->GhostParkSubsetIndexStart[i], m_LoadModel->GhostParkSubsetTexture[i], m_LoadModel->material,
			m_LoadModel->GhostParkSubsets[i], true, true, m_LoadModel->GhostParkVertPosArray[i], m_LoadModel->GhostParkVertIndexArray[i], SwapChain, d3d11Device))
			return false;
	}

	//Ghost model
	if (!m_LoadModel->LoadObjModel(L"Assets\\Models\\GhostJill.obj", &m_LoadModel->GhostVertBuff, &m_LoadModel->GhostIndexBuff, m_LoadModel->GhostSubsetIndexStart, m_LoadModel->GhostSubsetTexture, m_LoadModel->material,
		m_LoadModel->GhostSubsets, true, true, m_LoadModel->GhostVertPosArray, m_LoadModel->GhostVertIndexArray, SwapChain, d3d11Device))
		return false;

	//Fences Objs
	if (!m_LoadModel->LoadObjModel(L"Assets\\Models\\wood_fance_m3_t1.obj", &m_LoadModel->Fence_VertBuff, &m_LoadModel->Fence_IndexBuff, m_LoadModel->Fence_SubsetIndexStart, m_LoadModel->Fence_SubsetTexture,
		m_LoadModel->material, m_LoadModel->Fence_Subsets, true, true, m_LoadModel->Fence_VertPosArray, m_LoadModel->Fence_VertIndexArray, SwapChain, d3d11Device))
		return false;

	//MetalFences Objs
	if (!m_LoadModel->LoadObjModel(L"Assets\\Models\\GhostParkMetalFence_new.obj", &m_LoadModel->MetalFence_VertBuff, &m_LoadModel->MetalFence_IndexBuff, m_LoadModel->MetalFence_SubsetIndexStart, m_LoadModel->MetalFence_SubsetTexture,
		m_LoadModel->material, m_LoadModel->MetalFence_Subsets, true, true, m_LoadModel->MetalFence_VertPosArray, m_LoadModel->MetalFence_VertIndexArray, SwapChain, d3d11Device))
		return false;

	// Set up the tree positions then instance buffer
	srand(100);
	for (int i = 0; i < numGrass; i++)
	{
		float randX = ((float)(rand() % 1100) - 400);
		float randZ = ((float)(rand() % 1100) - 300);

		tempInstPos = XMVectorSet(randX*10, 0.0f, randZ*10, 0.0f);

		XMStoreFloat3(&InstPos[i], tempInstPos);
	}

	// Create Grass instance buffer
	D3D11_BUFFER_DESC instBuffDesc;
	ZeroMemory(&instBuffDesc, sizeof(instBuffDesc));

	instBuffDesc.Usage = D3D11_USAGE_DEFAULT;
	instBuffDesc.ByteWidth = sizeof(InstanceData) * numGrass;
	instBuffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	instBuffDesc.CPUAccessFlags = 0;
	instBuffDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA instData;
	ZeroMemory(&instData, sizeof(instData));

	instData.pSysMem = &InstPos[0];
	d3d11Device->CreateBuffer(&instBuffDesc, &instData, &m_LoadModel->GrassInstanceBuff);

	m_LoadModel->GrassWorld = XMMatrixIdentity();
	m_LoadModel->Scale = XMMatrixScaling(0.07f, 0.05f, 0.07f);
	m_LoadModel->GrassWorld = m_LoadModel->Scale;

	//------------------------------------------------------------------------------------------

	//HeightMap Setting
	//m_HeightMap->HeightMapLoad("Assets\\Textures\\heightmap01.bmp");
	//m_HeightMap->initHeightMap(d3d11Device, d3d11DevCon, VS_Buffer);

	//Model Setting
	//m_Model->initModel(d3d11Device, d3d11DevCon, VS_Buffer);

	//Init Camera
	m_Camera = new CameraClass(d3d11Device, d3d11DevCon);

	//Init Ui
	m_UI->initUI(d3d11Device, d3d11DevCon, VS_Buffer);
	m_UI->UpdateUI();

	//Create Bounding ------------------------------------------------------------------------------------------

	//Bounding volume for UI
	m_LoadModel->CreateBoundingVolumes(m_UI->button_VertPosArray, m_UI->button_BoundingBoxVertPosArray,
		m_UI->button_BoundingBoxVertIndexArray, m_UI->button_BoundingSphere, m_UI->button_CenterOffset);

	//init Bounding volume for key item
	m_LoadModel->CreateBoundingVolumes(m_LoadModel->InteractiveObj_VertPosArray, m_LoadModel->InteractiveObj_BoundingBoxVertPosArray,
		m_LoadModel->InteractiveObj_BoundingBoxVertIndexArray, m_LoadModel->InteractiveObj_BoundingSphere, m_LoadModel->InteractiveObj_CenterOffset);

	//init Bounding volume for fence
	m_LoadModel->CreateBoundingVolumes(m_LoadModel->Fence_VertPosArray, m_LoadModel->Fence_BoundingBoxVertPosArray,
		m_LoadModel->Fence_BoundingBoxVertIndexArray, m_LoadModel->Fence_BoundingSphere, m_LoadModel->Fence_CenterOffset);

	//init Bounding volume for Metalfence
	m_LoadModel->CreateBoundingVolumes(m_LoadModel->MetalFence_VertPosArray, m_LoadModel->MetalFence_BoundingBoxVertPosArray,
		m_LoadModel->MetalFence_BoundingBoxVertIndexArray, m_LoadModel->MetalFence_BoundingSphere, m_LoadModel->MetalFence_CenterOffset);

	//init Bounding volume for Park
	for (int i = 0; i < m_LoadModel->GhostPark_Num; i++) {
		m_LoadModel->CreateBoundingVolumes(m_LoadModel->GhostParkVertPosArray[i], m_LoadModel->GhostPark_BoundingBoxVertPosArray[i],
			m_LoadModel->GhostPark_BoundingBoxVertIndexArray[i], m_LoadModel->GhostPark_BoundingSphere[i], m_LoadModel->GhostPark_CenterOffset[i]);
	}

	//init skybox
	m_Sky = new SkyboxClass(d3d11Device);
	m_Sky->CreateSphere(10, 10);


	//init Textbox
	InitD2DScreenTexture();

	//Compile Shaders from shader file
	D3DX11CompileFromFile(L"Effects2.hlsl", 0, 0, "VS", "vs_4_0", 0, 0, 0, &VS_Buffer, 0, 0);
	D3DX11CompileFromFile(L"Effects2.hlsl", 0, 0, "PS", "ps_4_0", 0, 0, 0, &PS_Buffer, 0, 0);
	//Compile and crate light shader
	m_Light->initLight(d3d11Device);

	//Create the Shader Objects
	d3d11Device->CreateVertexShader(VS_Buffer->GetBufferPointer(), VS_Buffer->GetBufferSize(), NULL, &VS);
	d3d11Device->CreatePixelShader(PS_Buffer->GetBufferPointer(), PS_Buffer->GetBufferSize(), NULL, &PS);

	//Set Vertex and Pixel Shaders
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->PSSetShader(PS, 0, 0);
	//d3d11DevCon->PSSetShader(m_Light->PS_Ghost, 0, 0);

	//Create the Input Layout
	d3d11Device->CreateInputLayout(layout, numElements, VS_Buffer->GetBufferPointer(),
		VS_Buffer->GetBufferSize(), &vertLayout);

	// Create the grass Input Layout
	d3d11Device->CreateInputLayout(GrassLayout, numGrassElements, VS_Buffer->GetBufferPointer(),
		VS_Buffer->GetBufferSize(), &GrassVertLayout);

	//Set the Input Layout
	d3d11DevCon->IASetInputLayout(vertLayout);
	//Set Primitive Topology
	d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//Texture Setting
	//Load Texture file
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"Assets\\Textures\\grass.jpg", NULL, NULL, &groundTexture, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"Assets\\Textures\\title.png", NULL, NULL, &titleTexture, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"Assets\\Textures\\start.png", NULL, NULL, &playTexture, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"Assets\\Textures\\quit_butt.png", NULL, NULL, &quitTexture, NULL);

	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"Assets\\Textures\\blood_PNG6093.png", NULL, NULL, &EndBGTexture, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"Assets\\Textures\\restart.png", NULL, NULL, &RestartTexture, NULL);


	// Describe the Sample State
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	//Create the Sample State
	d3d11Device->CreateSamplerState(&sampDesc, &CubesTexSamplerState);

	//Camera setting
	m_Camera->InitCamera(Width, Height /*, m_HeightMap->collidableGeometryPositions, m_HeightMap->collidableGeometryIndices*/ );

	//Blending setting
	m_Blending = new BlendingClass(d3d11Device);
	m_Blending->initBlending();

	//Add shader for skybox
	m_Sky->initSky(m_Blending->cmdesc);

	//init Static and Interactive model
	m_LoadModel->initGroundAndPark();
	m_LoadModel->initInteractiveObj();
	m_LoadModel->initFence();
	m_LoadModel->initMetalFence();

	m_Picking = new PickingClass(hwnd, ClientWidth, ClientHeight);


	return true;

}
//---------------------------------------------------------UpdateScene------------------------------------------------------------------------------------------------------------------
void GraphicsClass::UpdateScene(bool LeftClick, bool RightClick, int pickWhat, int cdMethod, bool isWaling, bool W, bool S, bool A, bool D) {

	pickOptions = pickWhat;
	FlashLightOnOff = cdMethod;

	//Update Light
	m_Light->UpdateLight(m_Camera->camPosition, m_Camera->camTarget);
	//Update Skybox with cam pos
	m_Sky->UpdateSky(m_Camera->camPosition);

	//Update Dead UI
	if(isDead)
		m_UI->UpdateUIEnding(m_Camera->camPosition);

	//Checking for picking key items
	m_Picking->MouseClickDetectForPicking(LeftClick, RightClick,
		m_LoadModel->InteractiveObj_num,
		m_LoadModel->InteractiveObj_Hit,
		m_LoadModel->InteractiveObj_VertPosArray,
		m_LoadModel->InteractiveObj_VertIndexArray,
		m_LoadModel->InteractiveObj_World,
		m_Camera->camProjection, m_Camera->camView,
		m_LoadModel->InteractiveObj_CenterOffset,
		m_LoadModel->InteractiveObj_BoundingSphere,
		pickWhat,
		m_LoadModel->InteractiveObj_BoundingBoxVertPosArray,
		m_LoadModel->InteractiveObj_BoundingBoxVertIndexArray);

	//UI clicking
	m_Picking->MouseClickUI(isInUImode , LeftClick, m_UI->buttons_num, m_UI->buttons_Hit, m_UI->buttons, m_Camera->camProjection, m_Camera->camView, m_UI->button_BoundingBoxVertPosArray, m_UI->button_BoundingBoxVertIndexArray);

	//Maingame mechanics-----------------------------------------------------------------------------------------------------------------------
	if (m_Picking->score >= 1) {

		//Ghost is not spawn will increase time for spawning
		if (!isSpawn) {
			RandomSpawnTime++;
			//Reset Ghost Light
			m_Light->lightGhost.diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
		}

		//Time for Render jumpsacre
		if (RandomSpawnTime >= 1500.0f + AddTimeToSpwan) {
			if (!isJumpScare) {
				isJumpScare = true;
			}
			if (RandomSpawnTime >= 1600.0f + AddTimeToSpwan)
				isJumpScare = false;
		}

		//Ghost is spawn
		if (RandomSpawnTime >= 2000.0f + AddTimeToSpwan) {
			m_LoadModel->SpawnGhost(m_Camera->camPosition, isSpawn);
			isSpawn = true;
			isDead = false;
			RandomSpawnTime = 0.0f;
			//Update Light
			m_Light->UpdateLightGhost(m_LoadModel->GhostWorld);
			m_Light->lightGhost.diffuse = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
		}

		//Check whether  player is dead or not
		if (isSpawn && !isDead) {
			CountDownToDie++;
			HeartIsBeating = true;
			if (CountDownToDie >= 1700.0f - m_Picking->score * 200.0f) {
				//Turning on flashligh or walking = dead
				if (FlashLightOnOff == 0 || isWaling) {
					isDead = true;
					CountDownToDie = 0.0f;
				}
				//Turning off flashligh and no walking = alive
				if (FlashLightOnOff == 1 && !isWaling) {
					CountDownToDie = 0.0f;
					isSpawn = false;
					isDead = false;
					AddTimeToSpwan = ((float)(rand() % 2000)) + 1000.0f;
				}
				HeartIsBeating = false;
			}
		}
	}

	//Collected all five pages
	if (m_Picking->score == 5) {

		m_UI->buttons_Hit[1] = 0;

		for (int i = 0; i < m_LoadModel->InteractiveObj_num; i++)
		{
			m_LoadModel->InteractiveObj_Hit[i] = 0;
		}

		isSpawn = false;
		isDead = false;
		m_Picking->score = 0;

		//Reset player position
		m_Camera->camPosition = XMVectorSet(0.0f, 10.0f, -50.0f, 0.0f);
		m_Camera->camTarget = XMVectorSet(0.0f, 3.0f, 0.0f, 0.0f);
		m_Camera->camUp = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
		m_Camera->camView = XMMatrixLookAtLH(m_Camera->camPosition, m_Camera->camTarget, m_Camera->camUp);
	}

	//If player hit restart button
	if (isRestart) {
		for (int i = 0; i < m_LoadModel->InteractiveObj_num; i++)
		{
			m_LoadModel->InteractiveObj_Hit[i] = 0;
		}

		isSpawn = false;
		isDead = false;

		//Reset collected key item
		m_Picking->score = 0;

		//Reset Restart button
		m_UI->buttons_Hit[3] = 0;

	}

	is_W = W;
	is_S = S;
	is_A = A;
	is_D = D;

	//player and fence collision with Fence by AABB
	m_LoadModel->CalculateAABB_Player(m_LoadModel->Fence_BoundingBoxVertPosArray, m_Camera->camPosition, m_Camera->camBoundingBoxMinVertex, m_Camera->camBoundingBoxMaxVertex);

	for (int i = 0; i < 4; i++) 
		IsPlayerCollied[i] = false;

	//*****************************************************************Collision Fence**************************************************************************************
	for (int i = 0; i < m_LoadModel->Fence_num; i++) {
		if (m_LoadModel->BoundingBoxCollision(m_Camera->camBoundingBoxMinVertex, m_Camera->camBoundingBoxMaxVertex, m_LoadModel->Fence_BoundingBoxMinVertex[i], m_LoadModel->Fence_BoundingBoxMaxVertex[i]))
		{
			//Forward to fence W
			if (W) {
				IsPlayerCollied[0] = true;
				m_Camera->camPosition = XMVectorSetZ(m_Camera->camPosition, XMVectorGetZ(m_Camera->camPosition));

				//W-A
				if (A) {
					IsPlayerCollied[2] = true;
					m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
				}
				//W-D
				if (D) {
					IsPlayerCollied[3] = true;
					m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
				}
			}

			//Backward to fence S
			if (S) {
				IsPlayerCollied[1] = true;
				m_Camera->camPosition = XMVectorSetZ(m_Camera->camPosition, XMVectorGetZ(m_Camera->camPosition));

				//S-A
				if (A) {
					//IsPlayerCollied[2] = true;
					m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
				}
				//S-D
				if (D) {
					//IsPlayerCollied[3] = true;
					m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
				}
			}

			//Leftward to fence A
			if (A) {
				IsPlayerCollied[2] = true;
				m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
			}

			//Rightward to fence D
			if (D) {
				IsPlayerCollied[3] = true;
				m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
			}
		}
	}

	//*****************************************************************Collision MetalFence**************************************************************************************
	for (int i = 0; i < m_LoadModel->MetalFence_num; i++) 
	{
		if (m_LoadModel->BoundingBoxCollision(m_Camera->camBoundingBoxMinVertex, m_Camera->camBoundingBoxMaxVertex, m_LoadModel->MetalFence_BoundingBoxMinVertex[i], m_LoadModel->MetalFence_BoundingBoxMaxVertex[i]))
		{
			//Forward to fence W
			if (W) {
				IsPlayerCollied[0] = true;
				m_Camera->camPosition = XMVectorSetZ(m_Camera->camPosition, XMVectorGetZ(m_Camera->camPosition));

				//W-A
				if (A) {
					IsPlayerCollied[2] = true;
					m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
				}
				//W-D
				if (D) {
					IsPlayerCollied[3] = true;
					m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
				}
			}

			//Backward to fence S
			if (S) {
				IsPlayerCollied[1] = true;
				m_Camera->camPosition = XMVectorSetZ(m_Camera->camPosition, XMVectorGetZ(m_Camera->camPosition));

				//S-A
				if (A) {
					//IsPlayerCollied[2] = true;
					m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
				}
				//S-D
				if (D) {
					//IsPlayerCollied[3] = true;
					m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
				}
			}

			//Leftward to fence A
			if (A) {
				IsPlayerCollied[2] = true;
				m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
			}

			//Rightward to fence D
			if (D) {
				IsPlayerCollied[3] = true;
				m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
			}
		}
	}

	//*****************************************************************Collision Park**************************************************************************************
	
	for (int i = 0; i < m_LoadModel->GhostPark_Num; i++) {
		if (m_LoadModel->BoundingBoxCollision(m_Camera->camBoundingBoxMinVertex, m_Camera->camBoundingBoxMaxVertex, m_LoadModel->GhostPark_BoundingBoxMinVertex[i], m_LoadModel->GhostPark_BoundingBoxMaxVertex[i])) {
			//Forward to fence W
			if (W) {
				IsPlayerCollied[0] = true;
				m_Camera->camPosition = XMVectorSetZ(m_Camera->camPosition, XMVectorGetZ(m_Camera->camPosition));

				//W-A
				if (A) {
					IsPlayerCollied[2] = true;
					m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
				}
				//W-D
				if (D) {
					IsPlayerCollied[3] = true;
					m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
				}
			}

			//Backward to fence S
			else if (S) {
				IsPlayerCollied[1] = true;

				//S-A
				if (A) {
					IsPlayerCollied[2] = true;

				}
				//S-D
				if (D) {
					IsPlayerCollied[3] = true;

				}
			}

			//Leftward to fence A
			else if (A) {
				IsPlayerCollied[2] = true;
				m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
			}

			//Rightward to fence D
			else if (D) {
				IsPlayerCollied[3] = true;
				m_Camera->camPosition = XMVectorSetX(m_Camera->camPosition, XMVectorGetX(m_Camera->camPosition));
			}
		}
	}
}

//---------------------------------------------------------RenderText------------------------------------------------------------------------------------------------------------------
void GraphicsClass::RenderText(std::wstring text, ID3D11ShaderResourceView *d2dTexture, int inInt)
{
	//Release the D3D 11 Device
	keyedMutex11->ReleaseSync(0);

	//Use D3D10.1 device
	keyedMutex10->AcquireSync(0, 5);

	//Draw D2D content        
	D2DRenderTarget->BeginDraw();

	//Clear D2D Background
	D2DRenderTarget->Clear(D2D1::ColorF(0.0f, 0.0f, 0.0f, 0.0f));

	//PickOption
	std::wstring pckWhatStr;
	if (pickOptions == 0)
		pckWhatStr = L"Bounding Sphere";
	if (pickOptions == 1)
		pckWhatStr = L"Bounding Box";
	if (pickOptions == 2)
		pckWhatStr = L"NO Bounding";

	//Collision Method
	std::wstring FlashLightOnOrOff;
	if (FlashLightOnOff == 0)
		FlashLightOnOrOff = L"- ON";
	if (FlashLightOnOff == 1)
		FlashLightOnOrOff = L"- OFF";


	//Create our string
	std::wostringstream printString;
	printString << text << inInt << L"\n"
		<< L"Score: " << m_Picking->score << L"\n"
		<< L"Picked Dist: " << m_Picking->pickedDist << L"\n"
		<< L"FlashLightOnOff: " << FlashLightOnOrOff << L"\n"
		<< L"ClientWidth: " << m_Picking->ClientWidth << L"\n"
		<< L"ClientHeight: " << m_Picking->ClientHeight << L"\n"
		<< L"RandSpawnTime: " << RandomSpawnTime << L"\n"
		<< L"Die Time: " << CountDownToDie << L"\n"
		<< L"CamX: " << XMVectorGetX(m_Camera->camPosition) << L"\n"
		<< L"CamY: " << XMVectorGetY(m_Camera->camPosition) << L"\n"
		<< L"CamZ: " << XMVectorGetZ(m_Camera->camPosition) << L"\n"
		<< L"Coll W: " << IsPlayerCollied[0] << L"\n"
		<< L"Coll S: " << IsPlayerCollied[1] << L"\n"
		<< L"Coll A: " << IsPlayerCollied[2] << L"\n"
		<< L"Coll D: " << IsPlayerCollied[3] << L"\n"
		<< L"W: " << is_W << L"\n"
		<< L"S: " << is_S << L"\n"
		<< L"A: " << is_A << L"\n"
		<< L"D: " << is_D << L"\n";
		//<< L"GhostPark Subset: " << m_LoadModel->GhostParkSubsets << L"\n"
		//<< L"CheckvertPosIndexTemp: " << m_LoadModel->CheckvertPosIndexTemp << L"\n"
		//<< L"GhostPark VertPosArray: " << m_LoadModel->GhostParkVertPosArray.size() << L"\n";
		//<< L"MinCamX: " << XMVectorGetX(m_Camera->camBoundingBoxMinVertex) << L"\n"
		//<< L"MinCamY: " << XMVectorGetY(m_Camera->camBoundingBoxMinVertex) << L"\n"
		//<< L"MinCamZ: " << XMVectorGetZ(m_Camera->camBoundingBoxMinVertex) << L"\n"
		//<< L"MaxCamX: " << XMVectorGetX(m_Camera->camBoundingBoxMaxVertex) << L"\n"
		//<< L"MaxCamY: " << XMVectorGetY(m_Camera->camBoundingBoxMaxVertex) << L"\n"
		//<< L"MaxCamZ: " << XMVectorGetZ(m_Camera->camBoundingBoxMaxVertex) << L"\n";

		//<< L"Score: " << m_LoadModel->score << L"\n"
		//<< L"Collission Method: " << cdMethodString << L"\n"
		//<< L"CamX: " << XMVectorGetX(m_Camera->camPosition) << L"\n"
		//<< L"CamY: " << XMVectorGetY(m_Camera->camPosition) << L"\n"
		//<< L"CamZ: " << XMVectorGetZ(m_Camera->camPosition) << L"\n"
		//<< L"Check GePo: " << m_HeightMap->collidableGeometryPositions.size() << L"\n"
		//<< L"Check GePoIndex: " << m_HeightMap->collidableGeometryIndices.size() << L"\n"
		////<< L"Check GePo: " << m_LoadModel->collidableGeometryPositions_Ground.size() << L"\n"
		////<< L"Check GePoIndex: " << m_LoadModel->collidableGeometryIndices_Ground.size() << L"\n"
		//<< L"Num Trees To Draw: " << numTreesToDraw << L"\n"
		//<< L"RandSpawnTime: " << RandomSpawnTime << L"\n"
		//<< L"Die Time: " << CountDownToDie << L"\n"
		//<< L"END-------------: ";
	printText = printString.str();



	//Set the Font Color
	D2D1_COLOR_F FontColor = D2D1::ColorF(1.0f, 1.0f, 1.0f, 1.0f);

	//Set the brush color D2D will use to draw with
	Brush->SetColor(FontColor);

	//Create the D2D Render Area
	D2D1_RECT_F layoutRect = D2D1::RectF(0, 0, Width, Height);

	//Draw the Text
	D2DRenderTarget->DrawText(printText.c_str(), wcslen(printText.c_str()), TextFormat, layoutRect, Brush);

	D2DRenderTarget->EndDraw();

	//Release the D3D10.1 Device
	keyedMutex10->ReleaseSync(1);

	//Use the D3D11 Device
	keyedMutex11->AcquireSync(1, 5);

	//Set the blend state for D2D render target texture objects
	d3d11DevCon->OMSetBlendState(m_Blending->Transparency, NULL, 0xffffffff);

	//Set d2d's pixel shader so lighting calculations are not done
	d3d11DevCon->PSSetShader(m_Light->D2D_PS, 0, 0);

	//Set the d2d Index buffer
	d3d11DevCon->IASetIndexBuffer(d2dIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	//Set the d2d vertex buffer
	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	d3d11DevCon->IASetVertexBuffers(0, 1, &d2dVertBuffer, &stride, &offset);

	m_Camera->WVP = XMMatrixIdentity();
	m_Camera->cbPerObj.World = XMMatrixTranspose(m_Camera->WVP);
	m_Camera->cbPerObj.WVP = XMMatrixTranspose(m_Camera->WVP);
	d3d11DevCon->UpdateSubresource(m_Camera->cbPerObjectBuffer, 0, NULL, &m_Camera->cbPerObj, 0, 0);
	d3d11DevCon->VSSetConstantBuffers(0, 1, &m_Camera->cbPerObjectBuffer);
	d3d11DevCon->PSSetShaderResources(0, 1, &d2dTexture);
	d3d11DevCon->PSSetSamplers(0, 1, &CubesTexSamplerState);

	d3d11DevCon->RSSetState(m_Blending->CWcullMode);
	//Draw the text box
	d3d11DevCon->DrawIndexed(6, 0, 0);
}
//---------------------------------------------------------DrawSkybox------------------------------------------------------------------------------------------------------------------

void GraphicsClass::DrawSky(UINT stride, UINT offset) {

	//Set the spheres index buffer
	d3d11DevCon->IASetIndexBuffer(m_Sky->sphereIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	//Set the spheres vertex buffer
	d3d11DevCon->IASetVertexBuffers(0, 1, &m_Sky->sphereVertBuffer, &stride, &offset);

	m_Camera->WVP = m_Sky->sphereWorld * m_Camera->camView * m_Camera->camProjection;
	m_Camera->cbPerObj.WVP = XMMatrixTranspose(m_Camera->WVP);
	m_Camera->cbPerObj.World = XMMatrixTranspose(m_Sky->sphereWorld);
	d3d11DevCon->UpdateSubresource(m_Camera->cbPerObjectBuffer, 0, NULL, &m_Camera->cbPerObj, 0, 0);
	d3d11DevCon->VSSetConstantBuffers(0, 1, &m_Camera->cbPerObjectBuffer);
	//Send our skymap resource view to pixel shader
	d3d11DevCon->PSSetShaderResources(0, 1, &m_Sky->smrv);
	d3d11DevCon->PSSetSamplers(0, 1, &CubesTexSamplerState);
	//Set the new VS and PS shaders
	d3d11DevCon->VSSetShader(m_Sky->SKYMAP_VS, 0, 0);
	d3d11DevCon->PSSetShader(m_Sky->SKYMAP_PS, 0, 0);
	//Set the new depth/stencil and RS states
	d3d11DevCon->OMSetDepthStencilState(m_Sky->DSLessEqual, 0);
	d3d11DevCon->RSSetState(m_Sky->RSCullNone);
	d3d11DevCon->DrawIndexed(m_Sky->NumSphereFaces * 3, 0, 0);
	//Set the default VS shader and depth/stencil state
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->OMSetDepthStencilState(NULL, 0);

}

//---------------------------------------------------------DrawLoadedModel------------------------------------------------------------------------------------------------------------------
void GraphicsClass::DrawLoadedModel(XMMATRIX World, ID3D11Buffer* VertBuff, ID3D11Buffer* IndexBuff, int Subsets, std::vector<int> SubsetIndexStart,
	std::vector<int> SubsetTexture, UINT stride, UINT offset, bool isInstance, bool isGrass) {

	//Draw Loaded Model --------------------------------------------------------------------
	//Draw our model's subsets
	for (int i = 0; i < Subsets; ++i)
	{
		//Set the grounds index buffer
		d3d11DevCon->IASetIndexBuffer(IndexBuff, DXGI_FORMAT_R32_UINT, 0);
		//Set the grounds vertex buffer
		d3d11DevCon->IASetVertexBuffers(0, 1, &VertBuff, &stride, &offset);

		//Set the WVP matrix and send it to the constant buffer in effect file
		m_Camera->WVP = World * m_Camera->camView * m_Camera->camProjection;
		m_Camera->cbPerObj.WVP = XMMatrixTranspose(m_Camera->WVP);
		m_Camera->cbPerObj.World = XMMatrixTranspose(World);
		m_Camera->cbPerObj.difColor = m_LoadModel->material[SubsetTexture[i]].difColor;
		m_Camera->cbPerObj.hasTexture = m_LoadModel->material[SubsetTexture[i]].hasTexture;
		m_Camera->cbPerObj.hasNormMap = m_LoadModel->material[SubsetTexture[i]].hasNormMap;
		//Add--
		m_Camera->cbPerObj.isInstance = isInstance;
		m_Camera->cbPerObj.isGrass = isGrass;
		//---
		d3d11DevCon->UpdateSubresource(m_Camera->cbPerObjectBuffer, 0, NULL, &m_Camera->cbPerObj, 0, 0);
		d3d11DevCon->VSSetConstantBuffers(0, 1, &m_Camera->cbPerObjectBuffer);
		d3d11DevCon->PSSetConstantBuffers(1, 1, &m_Camera->cbPerObjectBuffer);
		if (m_LoadModel->material[SubsetTexture[i]].hasTexture)
			d3d11DevCon->PSSetShaderResources(0, 1, &m_LoadModel->meshSRV[m_LoadModel->material[SubsetTexture[i]].texArrayIndex]);
		d3d11DevCon->PSSetSamplers(0, 1, &CubesTexSamplerState);

		if (m_LoadModel->material[SubsetTexture[i]].hasNormMap)
			d3d11DevCon->PSSetShaderResources(1, 1, &m_LoadModel->meshSRV[m_LoadModel->material[SubsetTexture[i]].normMapTexArrayIndex]);

		d3d11DevCon->RSSetState(m_Sky->RSCullNone);
		int indexStart = SubsetIndexStart[i];
		int indexDrawAmount = SubsetIndexStart[i + 1] - SubsetIndexStart[i];
		if (!m_LoadModel->material[SubsetTexture[i]].transparent)
			d3d11DevCon->DrawIndexed(indexDrawAmount, indexStart, 0);
	}
}

//---------------------------------------------------------DrawInstance------------------------------------------------------------------------------------------------------------------
void GraphicsClass::DrawLoadedInstance(bool isDrawInUI) {

	UINT strides[2] = { sizeof(Vertex), sizeof(InstanceData) };
	UINT offsets[2] = { 0, 0 };

	ID3D11Buffer* vertInstBuffers[2] = { m_UI->quadVertBuffer, m_LoadModel->GrassInstanceBuff };
	d3d11DevCon->IASetInputLayout(GrassVertLayout);

	d3d11DevCon->IASetIndexBuffer(m_UI->quadIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	d3d11DevCon->IASetVertexBuffers(0, 2, vertInstBuffers, strides, offsets);

	//Set the WVP matrix and send it to the constant buffer in effect file
	m_Camera->WVP = m_LoadModel->GrassWorld * m_Camera->camView * m_Camera->camProjection;
	m_Camera->cbPerObj.WVP = XMMatrixTranspose(m_Camera->WVP);
	m_Camera->cbPerObj.World = XMMatrixTranspose(m_LoadModel->GrassWorld);
	m_Camera->cbPerObj.hasTexture = true;        
	m_Camera->cbPerObj.hasNormMap = false;    
	m_Camera->cbPerObj.isInstance = true;       
	m_Camera->cbPerObj.isGrass = true;
	d3d11DevCon->UpdateSubresource(m_Camera->cbPerObjectBuffer, 0, NULL, &m_Camera->cbPerObj, 0, 0);

	// We are sending two constant buffers to the vertex shader now, wo we will create an array of them
	ID3D11Buffer* vsConstBuffers[2] = { m_Camera->cbPerObjectBuffer, m_Camera->cbPerInstanceBuffer };
	d3d11DevCon->VSSetConstantBuffers(0, 2, vsConstBuffers);
	d3d11DevCon->PSSetConstantBuffers(1, 1, &m_Camera->cbPerObjectBuffer);
	d3d11DevCon->PSSetShaderResources(0, 1, &groundTexture);
	d3d11DevCon->PSSetSamplers(0, 1, &CubesTexSamplerState);

	d3d11DevCon->RSSetState(m_Sky->RSCullNone);
	d3d11DevCon->DrawIndexedInstanced(6, numGrassPerGroup * numGrass, 0, 0, 0);
	// Reset the default Input Layout
	d3d11DevCon->IASetInputLayout(vertLayout);

	if (!isDrawInUI) {
		/////Draw our grass instances/////
		for (int i = 0; i < m_LoadModel->GrassSubsets; ++i)
		{
			// Store the vertex and instance buffers into an array
			ID3D11Buffer* vertInstBuffers[2] = { m_LoadModel->GrassVertBuff, m_LoadModel->GrassInstanceBuff };

			//Set the models index buffer (same as before)
			d3d11DevCon->IASetIndexBuffer(m_LoadModel->GrassIndexBuff, DXGI_FORMAT_R32_UINT, 0);
			//Set the models vertex buffer
			d3d11DevCon->IASetVertexBuffers(0, 2, vertInstBuffers, strides, offsets);

			//Set the WVP matrix and send it to the constant buffer in effect file
			m_Camera->WVP = m_LoadModel->GrassWorld * m_Camera->camView * m_Camera->camProjection;
			m_Camera->cbPerObj.WVP = XMMatrixTranspose(m_Camera->WVP);
			m_Camera->cbPerObj.World = XMMatrixTranspose(m_LoadModel->GrassWorld);
			m_Camera->cbPerObj.difColor = m_LoadModel->material[m_LoadModel->GrassSubsetTexture[i]].difColor;
			m_Camera->cbPerObj.hasTexture = m_LoadModel->material[m_LoadModel->GrassSubsetTexture[i]].hasTexture;
			m_Camera->cbPerObj.hasNormMap = m_LoadModel->material[m_LoadModel->GrassSubsetTexture[i]].hasNormMap;
			m_Camera->cbPerObj.isInstance = true;        
			m_Camera->cbPerObj.isGrass = false;        
			d3d11DevCon->UpdateSubresource(m_Camera->cbPerObjectBuffer, 0, NULL, &m_Camera->cbPerObj, 0, 0);
			d3d11DevCon->VSSetConstantBuffers(0, 1, &m_Camera->cbPerObjectBuffer);
			d3d11DevCon->PSSetConstantBuffers(1, 1, &m_Camera->cbPerObjectBuffer);
			if (m_LoadModel->material[m_LoadModel->GrassSubsetTexture[i]].hasTexture)
				d3d11DevCon->PSSetShaderResources(0, 1, &m_LoadModel->meshSRV[m_LoadModel->material[m_LoadModel->GrassSubsetTexture[i]].texArrayIndex]);
			if (m_LoadModel->material[m_LoadModel->GrassSubsetTexture[i]].hasNormMap)
				d3d11DevCon->PSSetShaderResources(1, 1, &m_LoadModel->meshSRV[m_LoadModel->material[m_LoadModel->GrassSubsetTexture[i]].normMapTexArrayIndex]);
			d3d11DevCon->PSSetSamplers(0, 1, &CubesTexSamplerState);

			d3d11DevCon->RSSetState(m_Sky->RSCullNone);
			int indexStart = m_LoadModel->GrassSubsetIndexStart[i];
			int indexDrawAmount = m_LoadModel->GrassSubsetIndexStart[i + 1] - m_LoadModel->GrassSubsetIndexStart[i];
			if (!m_LoadModel->material[m_LoadModel->GrassSubsetTexture[i]].transparent)
				d3d11DevCon->DrawIndexedInstanced(indexDrawAmount, numGrass, indexStart, 0, 0);
	
		}
	}
}



//---------------------------------------------------------DrawScene------------------------------------------------------------------------------------------------------------------

void GraphicsClass::DrawScene() {

	//Clear our backbuffer
	float bgColor[4] = { (0.0f, 0.0f, 0.0f, 0.0f) };
	d3d11DevCon->ClearRenderTargetView(renderTargetView, bgColor);

	//Refresh the Depth/Stencil view
	d3d11DevCon->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//Draw Light
	m_Light->drawLight(d3d11DevCon);
	m_Light->light.ambient = XMFLOAT4(0.08f, 0.08f, 0.08f, 1.0f);


	//Test Turn on - off flashlight
	if (FlashLightOnOff == 1)
		m_Light->light.diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	else
		m_Light->light.diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);

	//Set our Render Target
	d3d11DevCon->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

	//Set the default blend state (no blending) for opaque objects
	d3d11DevCon->OMSetBlendState(0, 0, 0xffffffff);

	//Reset Vertex and Pixel Shaders
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->PSSetShader(PS, 0, 0);
	//d3d11DevCon->PSSetShader(m_Light->PS_Ghost, 0, 0);

	//Draw Grass instance
	DrawLoadedInstance(false);

	//Reset Vertex and Pixel Shaders
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->PSSetShader(PS, 0, 0);
	//d3d11DevCon->PSSetShader(m_Light->PS_Ghost, 0, 0);

	//Set the cubes vertex buffer
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	//Draw ground obj
	DrawLoadedModel(m_LoadModel->meshWorld, m_LoadModel->meshVertBuff, m_LoadModel->meshIndexBuff, m_LoadModel->meshSubsets, m_LoadModel->meshSubsetIndexStart,
		m_LoadModel->meshSubsetTexture,  stride,  offset, false, false);

	//Draw tree obj
	for (int i = 0; i < m_LoadModel->Tree_num; i++) {
		DrawLoadedModel(m_LoadModel->TreeWorld[i], m_LoadModel->TreeVertBuff, m_LoadModel->TreeIndexBuff, m_LoadModel->TreeSubsets, m_LoadModel->TreeSubsetIndexStart,
			m_LoadModel->TreeSubsetTexture, stride, offset, false, false);
	}

	//Draw GhostPark obj
	for (int i = 0; i < m_LoadModel->GhostPark_Num; i++) {
		DrawLoadedModel(m_LoadModel->GhostParkWorld[i], m_LoadModel->GhostParkVertBuff[i], m_LoadModel->GhostParkIndexBuff[i], m_LoadModel->GhostParkSubsets[i], m_LoadModel->GhostParkSubsetIndexStart[i],
			m_LoadModel->GhostParkSubsetTexture[i], stride, offset, false, false);
	}
	//Draw interactive objs
	for (int j = 0; j < m_LoadModel->InteractiveObj_num; j++) {
		if (m_LoadModel->InteractiveObj_Hit[j] == 0) {
			DrawLoadedModel(m_LoadModel->InteractiveObj_World[j], m_LoadModel->InteractiveObj_VertBuff, m_LoadModel->InteractiveObj_IndexBuff, m_LoadModel->InteractiveObj_Subsets,
				m_LoadModel->InteractiveObj_SubsetIndexStart, m_LoadModel->InteractiveObj_SubsetTexture, stride, offset, false, false);
		}
	}

	//Draw Ghost obj
	if (isSpawn) {
		DrawLoadedModel(m_LoadModel->GhostWorld, m_LoadModel->GhostVertBuff, m_LoadModel->GhostIndexBuff, m_LoadModel->GhostSubsets, m_LoadModel->GhostSubsetIndexStart,
			m_LoadModel->GhostSubsetTexture, stride, offset, false, false);
	}

	//Draw Fence objs
	for (int j = 0; j < m_LoadModel->Fence_num; j++) {
		DrawLoadedModel(m_LoadModel->Fence_World[j], m_LoadModel->Fence_VertBuff, m_LoadModel->Fence_IndexBuff, m_LoadModel->Fence_Subsets,
				m_LoadModel->Fence_SubsetIndexStart, m_LoadModel->Fence_SubsetTexture, stride, offset, false, false);
	}

	//Draw MetalFence objs
	for (int j = 0; j < m_LoadModel->MetalFence_num; j++) {
		DrawLoadedModel(m_LoadModel->MetalFence_World[j], m_LoadModel->MetalFence_VertBuff, m_LoadModel->MetalFence_IndexBuff, m_LoadModel->MetalFence_Subsets,
			m_LoadModel->MetalFence_SubsetIndexStart, m_LoadModel->MetalFence_SubsetTexture, stride, offset, false, false);
	}

	//Draw Skybox
	DrawSky(stride, offset);

	//Render Text
	RenderText(L"FPS: ", d2dTexture, m_Timer->fps);
	RenderText(L"", d2dTexture_crosshair, m_Timer->fps);

	//Render jumpsacre pic
	if(isJumpScare)
		RenderText(L"", d2dTexture_Scare, m_Timer->fps);

	//Present the backbuffer to the screen
	SwapChain->Present(0, 0);


}

//---------------------------------------------------------DrawUI------------------------------------------------------------------------------------------------------------------
void GraphicsClass::DrawUI() {
	//Clear our backbuffer
	float bgColor[4] = { (0.0f, 0.0f, 0.0f, 0.0f) };
	d3d11DevCon->ClearRenderTargetView(renderTargetView, bgColor);

	//Refresh the Depth/Stencil view
	d3d11DevCon->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//Draw Light
	m_Light->drawLight(d3d11DevCon);

	m_Light->light.diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	m_Light->light.ambient = XMFLOAT4(1.0f, 1.0f, 1.01f, 1.0f);


	//Set our Render Target
	d3d11DevCon->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

	//Set the default blend state (no blending) for opaque objects
	d3d11DevCon->OMSetBlendState(0, 0, 0xffffffff);

	//Reset Vertex and Pixel Shaders
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->PSSetShader(PS, 0, 0);

	DrawLoadedInstance(true);

	//Reset Vertex and Pixel Shaders
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->PSSetShader(PS, 0, 0);

	//Set the cubes index buffer
	d3d11DevCon->IASetIndexBuffer(m_UI->quadIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	//Set the cubes vertex buffer
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	d3d11DevCon->IASetVertexBuffers(0, 1, &m_UI->quadVertBuffer, &stride, &offset);

	for (int i = 0; i < m_UI->buttons_num; i++) {
		if (m_UI->buttons_Hit[i] == 0) {
			//Title of game
			if (i == 0)m_Camera->DrawCamera(m_UI->buttons[i], titleTexture, CubesTexSamplerState, m_Blending->CWcullMode, 6, true, false);
			//Start button
			if (i == 1)m_Camera->DrawCamera(m_UI->buttons[i], playTexture, CubesTexSamplerState, m_Blending->CWcullMode, 6, true, false);
			//Quit button
			if (i == 2)m_Camera->DrawCamera(m_UI->buttons[i], quitTexture, CubesTexSamplerState, m_Blending->CWcullMode, 6, true, false);
		}
	}

	SwapChain->Present(0, 0);
}

//---------------------------------------------------------DrawUI End------------------------------------------------------------------------------------------------------------------
void GraphicsClass::DrawUI_EndGame() {

	//Clear our backbuffer
	float bgColor[4] = { (0.0f, 0.0f, 0.0f, 0.0f) };
	d3d11DevCon->ClearRenderTargetView(renderTargetView, bgColor);

	//Refresh the Depth/Stencil view
	d3d11DevCon->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//Draw Light
	m_Light->drawLight(d3d11DevCon);

	m_Light->light.ambient = XMFLOAT4(1.0f, 1.0f, 1.01f, 1.0f);


	//Set our Render Target
	d3d11DevCon->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

	//Set the default blend state (no blending) for opaque objects
	d3d11DevCon->OMSetBlendState(0, 0, 0xffffffff);

	//Reset Vertex and Pixel Shaders
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->PSSetShader(PS, 0, 0);

	DrawLoadedInstance(true);

	//Reset Vertex and Pixel Shaders
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->PSSetShader(PS, 0, 0);

	//Set the cubes index buffer
	d3d11DevCon->IASetIndexBuffer(m_UI->quadIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	//Set the cubes vertex buffer
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	d3d11DevCon->IASetVertexBuffers(0, 1, &m_UI->quadVertBuffer, &stride, &offset);

	//Reset player position
	m_Camera->camPosition = XMVectorSet(0.0f, 10.0f, -50.0f, 0.0f);
	m_Camera->camTarget = XMVectorSet(0.0f, 3.0f, 0.0f, 0.0f);
	m_Camera->camUp = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	m_Camera->camView = XMMatrixLookAtLH(m_Camera->camPosition, m_Camera->camTarget, m_Camera->camUp);

	//End BG
	m_Camera->DrawCamera(m_UI->TitleEnd, EndBGTexture, CubesTexSamplerState, m_Blending->CWcullMode, 6, true, false);
	//Restart
	m_Camera->DrawCamera(m_UI->buttons[3], RestartTexture, CubesTexSamplerState, m_Blending->CWcullMode, 6, true, false);
	//Quit
	m_Camera->DrawCamera(m_UI->buttons[4], quitTexture, CubesTexSamplerState, m_Blending->CWcullMode, 6, true, false);

	SwapChain->Present(0, 0);

}
//---------------------------------------------------------CleanUp------------------------------------------------------------------------------------------------------------------

void GraphicsClass::CleanUp() {

	SwapChain->SetFullscreenState(false, NULL);
	PostMessage(hwnd, WM_DESTROY, 0, 0);

	//Release InitD3D11
	SwapChain->Release();
	d3d11Device->Release();
	d3d11DevCon->Release();
	renderTargetView->Release();

	//Release shaders
	VS->Release();
	PS->Release();
	VS_Buffer->Release();
	PS_Buffer->Release();
	vertLayout->Release();
	GrassVertLayout->Release();

	//Release 3D Textures
	groundTexture->Release();
	CubesTexSamplerState->Release();
	//startBGTexture->Release();
	titleTexture->Release();
	playTexture->Release();
	quitTexture->Release();
	EndBGTexture->Release();
	//EndtitleTexture->Release();
	RestartTexture->Release();

	//Release Depth
	depthStencilView->Release();
	depthStencilBuffer->Release();

	//Release d2D
	d2dVertBuffer->Release();
	d2dIndexBuffer->Release();

	d3d101Device->Release();
	keyedMutex11->Release();
	keyedMutex10->Release();
	D2DRenderTarget->Release();
	Brush->Release();
	BackBuffer11->Release();
	sharedTex11->Release();
	DWriteFactory->Release();
	TextFormat->Release();

	//Release 2D Textures
	d2dTexture->Release();
	d2dTexture_crosshair->Release();
	d2dTexture_blood->Release();
	d2dTexture_Scare->Release();

	m_Camera->CleanUp();
	//m_Model->CleanUp();
	//m_HeightMap->CleanUp();
	m_Blending->CleanUp();
	m_Light->CleanUp();
	m_Sky->CleanUp();
	m_LoadModel->CleanUp();
	m_UI->CleanUp();

	//Delete new obj
	delete m_Camera;
	m_Camera = 0;
	delete m_Blending;
	m_Blending = 0;
	delete m_Timer;
	m_Timer = 0;
	delete m_Light;
	m_Light = 0;
	delete m_Sky;
	m_Sky = 0;
	delete m_LoadModel;
	m_LoadModel = 0;
	delete m_Picking;
	m_Picking = 0;
	delete m_UI;
	m_UI = 0;
}


