#pragma once
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib, "d3dx10.lib")


//lib for wirte
#pragma comment (lib, "D3D10_1.lib")
#pragma comment (lib, "DXGI.lib")
#pragma comment (lib, "D2D1.lib")
#pragma comment (lib, "dwrite.lib")

//lib for input
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")

//lib for sound
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "winmm.lib")

#include <windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>
#include <xnamath.h>

//include for write
#include <D3D10_1.h>
#include <DXGI.h>
#include <D2D1.h>
#include <sstream>
#include <dwrite.h>

//include for input
#include <dinput.h>

//Load obj
#include <vector>
#include <fstream>
#include <istream>

//Sound
#include <mmsystem.h>
#include <dsound.h>
#include <stdio.h>

//DirectX Tool Kits
//#include "CommonStates.h"
//#include "DDSTextureLoader.h"
//#include "DirectXHelpers.h"
//#include "Effects.h"
//#include "GamePad.h"
//#include "GeometricPrimitive.h"
//#include "GraphicsMemory.h"
//#include "Keyboard.h"
//#include "Model.h"
//#include "Mouse.h"
//#include "PostProcess.h"
//#include "PrimitiveBatch.h"
//#include "ScreenGrab.h"
//#include "SimpleMath.h"
//#include "SpriteBatch.h"
//#include "SpriteFont.h"
//#include "VertexTypes.h"
//#include "WICTextureLoader.h"


//#include <CommonStates.h>
//#include <DDSTextureLoader.h>
//#include <DirectXHelpers.h>
//#include <Effects.h>
//#include <GamePad.h>
//#include <GeometricPrimitive.h>
//#include <GraphicsMemory.h>
//#include <Keyboard.h>
//#include <Model.h>
//#include <Mouse.h>
//#include <PostProcess.h>
//#include <PrimitiveBatch.h>
//#include <ScreenGrab.h>
//#include <SimpleMath.h>
//#include <SpriteBatch.h>
//#include <SpriteFont.h>
//#include <VertexTypes.h>
//#include <WICTextureLoader.h>