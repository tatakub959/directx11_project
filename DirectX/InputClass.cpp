#include "inputclass.h"

InputClass::InputClass(HINSTANCE hInstanceM, HWND hwndM) {
	hInstance = hInstanceM;
	hwnd = hwndM;
}

bool InputClass::InitDirecInput() {


	DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&DirectInput, NULL);


	DirectInput->CreateDevice(GUID_SysKeyboard, &DIKeyboard, NULL);


	DirectInput->CreateDevice(GUID_SysMouse, &DIMouse, NULL);

	DIKeyboard->SetDataFormat(&c_dfDIKeyboard);
	DIKeyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	DIMouse->SetDataFormat(&c_dfDIMouse);
	DIMouse->SetCooperativeLevel(hwnd, DISCL_NONEXCLUSIVE | DISCL_NOWINKEY | DISCL_FOREGROUND);

	return true;

}

void InputClass::DetectInput(double time, bool isPlayerColl[] , bool gameIsRun) {

	DIMOUSESTATE mouseCurrState;

	BYTE keyboardState[256];

	DIKeyboard->Acquire();
	DIMouse->Acquire();

	DIMouse->GetDeviceState(sizeof(DIMOUSESTATE), &mouseCurrState);

	DIKeyboard->GetDeviceState(sizeof(keyboardState), (LPVOID)&keyboardState);

	float speed = 1.0f * time;

	isRunning = false;
	isWalking = false;
	isLanding = false;

	//Direction
	is_Forward = false;
	is_Backward = false;
	is_Right = false;
	is_left = false;

	if (gameIsRun) {
		SetCursorPos(1920/2, 40);
		ShowCursor(false);

		if (keyboardState[DIK_ESCAPE] & 0x80)
			PostMessage(hwnd, WM_DESTROY, 0, 0);
		//HIT "W"-------------------------------------------
		else if (keyboardState[DIK_W] & 0x80)
		{
			//Direction
			is_Forward = true;
			is_Backward = false;

			if (!isPlayerColl[0])
				moveBackForward += speed;
			else
				moveBackForward = -0.1f * time;

			isRunning = false;
			isWalking = true;
			if (moveBackForward > time * 1.0f) {
				//Static Speed (Normal speed when moving forward)
				moveBackForward = time* 15.0f;

				//Check if press LSHIFT (Run) speed will be increased
				if (keyboardState[DIK_LSHIFT] & 0x80) {
					moveBackForward = time* 50.0f;
					isRunning = true;
					isWalking = false;
					//Add diagonal direction with running
					if (keyboardState[DIK_A] & 0x80)
						moveLeftRight = -time * 10.0f;

					else if (keyboardState[DIK_D] & 0x80)
						moveLeftRight = +time * 10.0f;

					else
						moveLeftRight = 0.0f;
				}

				//Check if press LCTRL (Crouch) speed will be decreased
				else if (keyboardState[DIK_LCONTROL] & 0x80) {
					moveBackForward = time* 500.0f;

					//Add diagonal direction with running
					if (keyboardState[DIK_A] & 0x80)
						moveLeftRight = -time * 5.0f;

					else if (keyboardState[DIK_D] & 0x80)
						moveLeftRight = +time * 5.0f;

					else
						moveLeftRight = 0.0f;
				}

				//Add diagonal direction without running 
				else {
					if (keyboardState[DIK_A] & 0x80) {
						is_left = true;
						if (!isPlayerColl[2])
							moveLeftRight = -time * 10.0f;
						else
							moveLeftRight = 0.0f;
						//moveLeftRight = 0.01f * time;
					}

					else if (keyboardState[DIK_D] & 0x80) {
						is_Right = true;
						if (!isPlayerColl[3])
							moveLeftRight = +time * 10.0f;
						else
							moveLeftRight = 0.0f;
						//moveLeftRight = -0.01f * time;
					}

					else
						moveLeftRight = 0.0f;
				}
			}
		}
		//HIT "S"-------------------------------------------
		else if (keyboardState[DIK_S] & 0x80)
		{
			//Direction
			is_Forward = false;
			is_Backward = true;

			if (!isPlayerColl[1])
				moveBackForward -= speed;
			else
				moveBackForward = 0.1f * time;

			isWalking = true;
			if (moveBackForward < 0.0f) {
				moveBackForward = -time * 10.0f;

				//Add diagonal direction with backward
				if (keyboardState[DIK_A] & 0x80) {
					is_left = true;
					moveLeftRight = -time * 7.0f;
				}

				else if (keyboardState[DIK_D] & 0x80) {
					is_Right = true;
					moveLeftRight = +time * 7.0f;
				}

				else
					moveLeftRight = 0.0f;
			}
		}
		//HIT "A"-------------------------------------------
		else if (keyboardState[DIK_A] & 0x80)
		{
			//Direction

			is_Right = false;
			is_left = true;

			if (!isPlayerColl[2])
				moveLeftRight -= speed;
			else
				moveLeftRight = 0.1f * time;

			isWalking = true;
			if (moveLeftRight < 0.0f) {
				moveLeftRight = -time * 10.0f;
				moveBackForward = 0.0f;
			}

		}
		//HIT "D"-------------------------------------------
		else if (keyboardState[DIK_D] & 0x80)
		{
			//Direction
			is_Right = true;
			is_left = false;

			if (!isPlayerColl[3])
				moveLeftRight += speed;
			else
				moveLeftRight = -0.1f * time;

			isWalking = true;
			if (moveLeftRight > time * 1.0f) {
				moveLeftRight = time* 10.0f;
				moveBackForward = 0.0f;
			}
		}

		else {

			moveLeftRight = 0;
			moveBackForward = 0;

		}

		//HIT "Space bar"----------------------------------
		if (keyboardState[DIK_SPACE] & 0x80)
		{
			isJump = true;
			isRunning = false;
			isWalking = false;
			//TimeForOneJump += speed;
			//if (TimeForOneJump > time*15.0f) {
			//	isJump = false;
			//	isLanding = true;
			//}


		}
		if (!(keyboardState[DIK_SPACE] & 0x80)) {
			isJump = false;
			TimeForOneJump = 0.0f;

		}

		//HIT "P"----------------------------------
		if (keyboardState[DIK_P] & 0x80)
		{
			if (!isPDown)
			{
				pickWhat++;
				if (pickWhat == 3)
					pickWhat = 0;
				isPDown = true;
			}
		}
		if (!(keyboardState[DIK_P] & 0x80)) {
			isPDown = false;
		}

		//HIT "C"----------------------------------
		if (keyboardState[DIK_F] & 0x80)
		{
			if (!isCDown)
			{
				cdMethod++;
				if (cdMethod == 2)
					cdMethod = 0;
				isCDown = true;
			}
		}
		if (!(keyboardState[DIK_F] & 0x80))
		{
			isCDown = false;
		}


		//Rotate Camera with mouse
		if ((mouseCurrState.lX != mouseLastState.lX) || (mouseCurrState.lY != mouseLastState.lY))
		{
			camYaw += mouseLastState.lX * 0.001f;

			camPitch += mouseCurrState.lY * 0.001f;

			if (camPitch > 0.85f)
				camPitch = 0.85f;
			if (camPitch < -0.85f)
				camPitch = -0.85f;

			mouseLastState = mouseCurrState;
		}
	}
	else
		ShowCursor(true);

	//Left click to pick
	if (mouseCurrState.rgbButtons[0])
	{
		LeftClick = true;
		//TimeForOneClick += speed;
		//if (TimeForOneClick > time*2.0f)
		//	LeftClick = false;
	}
	if (!mouseCurrState.rgbButtons[0])
	{
		LeftClick = false;
		TimeForOneClick = 0.0f;
	}

	//Right click to reset picking
	if (mouseCurrState.rgbButtons[1]) {
		RightClick = true;
	}
	if (!mouseCurrState.rgbButtons[1]) {
		RightClick = false;
	}

	return;
}


void InputClass::CleanUp() {

	DIKeyboard->Unacquire();
	DIMouse->Unacquire();
	DirectInput->Release();
}