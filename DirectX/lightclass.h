#pragma once
#include "d3d11class.h"

class LightClass {
public:

	void initLight(ID3D11Device* d3d11Device);
	void drawLight(ID3D11DeviceContext* d3d11DevCon);
	void UpdateLight(XMVECTOR camPosition , XMVECTOR camTarget);
	void UpdateLightGhost(XMMATRIX GhostWorld);
	void CleanUp();

public:

	ID3D11Buffer* cbPerFrameBuffer;
	ID3D11PixelShader* D2D_PS;
	ID3D10Blob* D2D_PS_Buffer;

	//ID3D11PixelShader* PS_Ghost;
	//ID3D10Blob* PS_Ghost_Buffer;

	struct Light
	{
		Light()
		{
			ZeroMemory(this, sizeof(Light));
		}
		//Direct --------------------
		//XMFLOAT3 dir;
		//float pad;
		//XMFLOAT4 ambient;
		//XMFLOAT4 diffuse;

		////Point Light ---------------
		//XMFLOAT3 dir;

		//float pad1;
		//XMFLOAT3 pos;
		//float range;
		//XMFLOAT3 att;
		//float pad2;

		//XMFLOAT4 ambient;
		//XMFLOAT4 diffuse;

		//Spot Light---------------
		XMFLOAT3 pos;
		float range;
		XMFLOAT3 dir;
		float cone;
		XMFLOAT3 att;
		float pad2;
		XMFLOAT4 ambient;
		XMFLOAT4 diffuse;

		//XMFLOAT3 DecorPos[9];
	};

	Light light;
	Light lightGhost;
	Light DecoratedLight;
	int DecorLight_num = 9;

	struct cbPerFrame
	{
		Light  light;
		Light lightGhost;
		Light DecoratedLight;
	};

	cbPerFrame constbuffPerFrame;

};