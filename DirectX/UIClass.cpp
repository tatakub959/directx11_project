#include "uiclass.h"
void UIclass::initUI(ID3D11Device* d3d11Device, ID3D11DeviceContext* d3d11DevCon, ID3D10Blob* VS_Buffer) {

	// Create quad geometry------------------------------------
	Vertex v[] =
	{
		// Front Face
			Vertex(-1.0f, -1.0f, -1.0f, 0.0f, 1.0f,-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
			Vertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f,-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
			Vertex(1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
			Vertex(1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
	};

	for(int i = 0; i < sizeof(v)/sizeof(*v); i++)
		button_VertPosArray.push_back(v[i].pos);

	DWORD indices[] = {
		// Front Face
		0,  1,  2,
		0,  2,  3,
	};

	for (int j = 0; j < sizeof(indices)/sizeof(*indices); j++)
		button_VertIndexArray.push_back(indices[j]);

	D3D11_BUFFER_DESC indexBufferDesc;
	ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(DWORD) * 2 * 3;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA iinitData;

	iinitData.pSysMem = indices;
	d3d11Device->CreateBuffer(&indexBufferDesc, &iinitData, &quadIndexBuffer);


	D3D11_BUFFER_DESC vertexBufferDesc;
	ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(Vertex) * 4;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;

	ZeroMemory(&vertexBufferData, sizeof(vertexBufferData));
	vertexBufferData.pSysMem = v;
	d3d11Device->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &quadVertBuffer);
}
void UIclass::UpdateUI() {

	
	for (int i = 0; i < buttons_num; i++) {

		buttons_Hit[i] = 0;

		Scale = XMMatrixScaling(5.0f, 2.0f, 1.0f);

		buttons[i] = XMMatrixIdentity();
		//Title
		if (i == 0) {
			Scale = XMMatrixScaling(20.0f, 8.0f, 1.0f);
			Translation = XMMatrixTranslation(0.0f, 3.0f, -20.0f);
		}
		//Start
		else if (i == 1) Translation = XMMatrixTranslation(0.0f, 4.0f, -20.0f);
		
		//Quit
		else if (i == 2) Translation = XMMatrixTranslation(0.0f, 0.0f, -20.0f);
		
		buttons[i] = Translation * Scale;
	}
}

void UIclass::UpdateUIEnding(XMVECTOR camPosition) {
	//BG
	TitleEnd = XMMatrixIdentity();
	Scale = XMMatrixScaling(140.0f, 90.0f, 1.0f);
	Translation = XMMatrixTranslation(0.0f, 0.0f, XMVectorGetZ(camPosition) + 100.0f);
	TitleEnd = Translation * Scale;

	//Restart
	buttons[3] = XMMatrixIdentity();
	Scale = XMMatrixScaling(5.0f, 2.0f, 1.0f);
	Translation = XMMatrixTranslation(0.0f, 6.0f, XMVectorGetZ(camPosition) + 20.0f);
	buttons[3] = Translation * Scale;

	//Quit
	buttons[4] = XMMatrixIdentity();
	Scale = XMMatrixScaling(5.0f, 2.0f, 1.0f);
	Translation = XMMatrixTranslation(0.0f, 1.0f, XMVectorGetZ(camPosition) + 20.0f);
	buttons[4] = Translation * Scale;
}
void UIclass::CleanUp() {
	quadVertBuffer->Release();
	quadIndexBuffer->Release();
}