#pragma once
#include "d3d11class.h"


class InputClass {

public:

	InputClass(HINSTANCE hInstance, HWND hwnd);
	bool InitDirecInput();
	void DetectInput(double time , bool isPlayerColl[] , bool gameIsRun);
	void CleanUp();

public:

	HINSTANCE hInstance;
	HWND hwnd;

	IDirectInputDevice8* DIKeyboard;
	IDirectInputDevice8* DIMouse;

	DIMOUSESTATE mouseLastState;
	LPDIRECTINPUT8 DirectInput;


	float rotx = 0;
	float rotz = 0;
	float scaleX = 1.0f;
	float scaleY = 1.0f;

	XMMATRIX Rotationx;
	XMMATRIX Rotationz;

	float moveLeftRight = 0.0f;
	float moveBackForward = 0.0f;

	//Rotate mouse
	float camYaw = 0.0f;
	float camPitch = 0.0f;

	//Left mouse
	bool LeftClick = false;

	//Right mouse
	bool RightClick = false;
	float TimeForOneClick = 0.0f;

	//Bounding Methods
	int pickWhat = 0;
	bool isPDown = false;

	//Collision Method
	int cdMethod = 0;
	bool isCDown = false;

	//Jump
	bool isJump = false;
	float TimeForOneJump = 0.0f;

	//Walk - Run - landing(Jump)
	bool isRunning;
	bool isWalking;
	bool isLanding;

	//Check Direction
	bool is_Forward;
	bool is_Backward;
	bool is_Right;
	bool is_left;

};